from PySide.QtCore import *
from PySide.QtGui import *
from widgets import main_window_UIs, waiting_dialog_UIs, amg_tree_widget, task_list
import packer_worker
from packer import amg_packer
from icons import icons_rcs
from amg import icons
import amg_config
import os


class AMGPackerWindow(QMainWindow, main_window_UIs.Ui_MainWindow):
    def __init__(self):
        super(AMGPackerWindow, self).__init__()
        self.setupUi(self)
        style = os.path.join(os.path.dirname(__file__), 'style','style.css')
        # self.setStyleSheet(open(style).read())
        self.setWindowIcon(QIcon(icons.ico['animagrad']))
        self.tree_studio = amg_tree_widget.AMGTreeWidget(True, parent=self)
        self.studio_tree_ly.addWidget(self.tree_studio)
        self.tree_user = amg_tree_widget.AMGTreeWidget(True, parent=self)
        self.user_tree_ly.addWidget(self.tree_user)
        self.out = task_list.AMGtaskList()
        self.tasks_ly.addWidget(self.out)
        # variables
        self.users = []

        self.update_ui()
        # connects
        # self.projects_cbb.currentIndexChanged.connect(self.update_studio_tree)
        self.users_cbb.currentIndexChanged.connect(self.update_user_tree)
        self.upload_btn.clicked.connect(self.submit_upload)
        self.download_btn.clicked.connect(self.submit_download)

        # start
        self.splitter_2.setSizes([1000,100])
        dial = self.open_waiting_dialog('Connect to Shotgun server', icons.ico['shotgun'])
        self.load_config()
        self.update_studio_tree()
        self.update_user_tree()
        dial.close()


    def open_waiting_dialog(self, text='Waiting...', icon=icons.ico['clock']):
        dial = Waiting_dialog(self)
        dial.set_text(text)
        dial.set_icon(icon)
        dial.show()
        dial.repaint()
        return dial


    def update_ui(self):
        data = packer_worker.get_init_data()
        self.users = data['users']
        self.users_cbb.clear()
        for i, usr in enumerate(self.users):
            self.users_cbb.addItem(usr.name)
            self.users_cbb.setItemData(i, usr)

    def load_config(self):
        pass

    def switch_user_list_tree(self):
        self.user_list.setVisible(self.new_btn.isChecked())
        self.user_tree.setVisible(not self.new_btn.isChecked())
        self.tree_active = not self.new_btn.isChecked()

    def update_studio_tree(self):
        root = amg_config.conf['projects_path']
        res = self.tree_studio.fill(root)
        self.upload_btn.setEnabled(bool(res))
        self.download_btn.setEnabled(bool(res))

    def update_user_tree(self):
        usr = self.users_cbb.itemData(self.users_cbb.currentIndex())
        self.tree_user.fill(usr.studio_path)

    def status_message(self, msg, timeout=5000):
        self.statusbar.showMessage(msg, timeout)

    ################# SUBMIT

    def submit_upload(self, *args):
        files = self.tree_studio.all_selected_data()
        per_projects = {}
        for f in files:
            if f.project in per_projects:
                per_projects[f.project].append(f)
            else:
                per_projects[f.project] = [f]
        user = self.users_cbb.itemData(self.users_cbb.currentIndex())
        for project, files in per_projects.items():
            p = amg_packer.AmgPacker(project, user)
            files, errors = p.collect_files(files)
            if errors:
                self.out.message('Error files: %s' % len(errors))
                self.out.message('\n'.join([x for x in errors]))
            self.out.message('Files: %s' % len(files))
            p.copy_files_to_user(files, message_callback=self.out.message)
        self.update_user_tree()

    def submit_download(self, *args):
        print self.tree_user.all_selected_data()


class Waiting_dialog(QDialog, waiting_dialog_UIs.Ui_waiting_dialod):
    def __init__(self, parent):
        super(Waiting_dialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowIcon(QIcon(icons.ico['animagrad']))
        self.icon_lb.setText('')

    def showEvent(self, *args, **kwargs):
        self.repaint()
        super(Waiting_dialog, self).showEvent(*args, **kwargs)

    def set_text(self, text):
        self.message_lb.setText(text)

    def set_icon(self, path):
        if not path:
            self.icon_lb.setPixmap(QPixmap())
        else:
            self.icon_lb.setPixmap(QPixmap(path).scaled(64,64,Qt.KeepAspectRatio, Qt.SmoothTransformation))

class TaskManager(QObject):

    def __init__(self):
        super(TaskManager, self).__init__()


if __name__ == '__main__':
    app = QApplication([])
    w = AMGPackerWindow()
    w.show()
    app.exec_()