import os, shutil, re, json, subprocess, fileinput, datetime
from amg import api
from amg.shotgun import amg_shoutgun as ams
from amg.api.amg_path import join
try:
    import amg_file_manager
except:
    from .. import amg_file_manager

reload(amg_file_manager)
reload(api)

locked_ext = ['hda', 'otl', 'ma', 'mb']


class AmgPacker(object):
    def __init__(self):
        super(AmgPacker, self).__init__()
        # self.project = project # amg.shogun.amg_shotgun.SG_Project
        # self.user = user # amg.api.amg_user.User
        self.files = list()
        self.errors = list()
        self.errors_msg = []
        self._checked = list()

    @classmethod
    def share(cls, files, user, filter_include=None, filter_exclude=None):
        if not project or not files:
            raise Exception('You need to specify Project and Files')
        p = cls(project, user)
        # collect
        allfiles, errors = p.collect_files(files, filter_include, filter_exclude)
        # copy
        if not isinstance(user, api.amg_user.User) and isinstance(user, (str, unicode)):
            user = api.amg_user.User(user)
        p.copy_files_to_user(allfiles)

    def collect_files(self, files, filter_inclide=None, filter_exclude=None):
        self.files = list()
        self.errors = list()
        self._checked = list()
        for f in files:
            self.find_depended_files(f.path if isinstance(f, amg_file_manager.FM) else f, filter_inclide, filter_exclude)
        return self.files, self.errors

    def copy_files_to_user(self, files, user, force=False, message_callback=None):
        projects = []
        for file in files:
            if message_callback:
                message_callback(file)
            else:
                print file
            dist = file.get_ftp_path(user)
            if os.path.exists(dist):
                if force:
                    try:
                        os.remove(dist)
                    except:
                        self.errors_msg.append('Cant remove file: %s' % dist)
                else:
                    if int(os.stat(file.path).st_mtime) > int(os.stat(dist).st_mtime):
                        try:
                            os.remove(dist)
                        except:
                            self.errors_msg.append('Cant remove old file: %s' % dist)
                    else:
                        continue
            err = file.repath_to_user(user)
            if err:
                self.errors_msg.append('Cant remove old file: %s' % dist)
            if not file.project in projects:
                projects.append(file.project)
        # update projects meta
        for prj in projects:
            if prj.meta:
                copy_meta = os.path.join(user.studio_path, prj.code, ams.project_meta_file).replace('\\','/')
                shutil.copy2(prj.meta['path'], copy_meta)

        return self.errors_msg or None

    def copy_files_to_studio(self, files, user):
        for file in files:
            file.repath_to_studio(user)

    def collect_user_files(self, user, not_opened=True):
        user_files = []
        search_path = user.studio_path# + '/' + self.project.code
        for path, dirs, files in os.walk(search_path):
            for f in files:
                if os.path.splitext(f)[-1] == amg_file_manager.Meta.suffix:
                    meta = amg_file_manager.Meta(os.path.join(path, f))
                    if not meta['owner'] == amg_file_manager.Meta.STUDIO:# \
                            #and not meta['opened']:
                        user_files.append(amg_file_manager.FM.create(meta.file))
        if not_opened:
            user_files = [x for x in user_files if not x.meta['opened']]
        return user_files

    def find_depended_files(self, file, filter_include=None, filter_exclude=None):
        # skip checked
        if file in self._checked:
            return
        else:
            self._checked.append(file)
        # sequence
        seq = self.extract_sequence(file)
        if seq:
            # filter
            if filter_exclude:
                seq = [x for x in seq if not os.path.splitext(x)[-1].strip('.') in filter_exclude]
            if filter_include:
                seq = [x for x in seq if os.path.splitext(x)[-1].strip('.') in filter_include]
            for s in seq:
                self.add_file(s, True)
            return
        if not os.path.exists(file):
            if not file in self.errors:
                self.errors.append(file)
            return
        # scene
        fm = self.add_file(file)
        if not fm:
            return
        new_files = fm.extract()
        # filter
        if filter_exclude:
            new_files = [x for x in new_files if not os.path.splitext(x)[-1].strip('.') in filter_exclude or os.path.splitext(x)[-1].strip('.') in locked_ext]
        if filter_include:
            new_files = [x for x in new_files if os.path.splitext(x)[-1].strip('.') in filter_include  or os.path.splitext(x)[-1].strip('.') in locked_ext]
        # add to files
        for f in new_files:
            self.find_depended_files(f, filter_include, filter_exclude)

    def add_file(self, path, add_to_error=False):
        if os.path.exists(path):
            if not path in [x.path for x in self.files]:
                nfm = amg_file_manager.FM.create(path)
                self.files.append(nfm)
                return nfm
        else:
            if not path in self.errors:
                self.errors.append(path)

    def extract_sequence(self, path):
        # udim
        dir = os.path.dirname(path)
        if not os.path.exists(dir):
            return
        bn = os.path.basename(path)
        name, ext = os.path.splitext(bn)
        matchname = name
        if '<udim>' in path.lower():
            matchname = matchname.replace('<udim>', r'\d+')
            matchname = matchname+'.(tx|%s)' % ext[1:]
        # %04d
        for m in re.findall(r"(%(\d+)\w)", matchname):
            matchname = matchname.replace(m[0], '\d{%s}' % int(m[1]))
        # ####
        for m in re.findall(r"#+", matchname):
            matchname = matchname.replace(m, '\d{%s}' % len(m))

        # $F4
        for m in re.findall(r"\$[A-Z0-9_]+", matchname):
            matchname = matchname.replace(m, '.*?')
        # `(expr)`
        for m in re.findall(r"`.*?`", matchname):
            matchname = matchname.replace(m, '.*?')
        # search
        # print 'PATH: %s | NAME: %s | DIR: %s' % (path, matchname, dir)
        if not matchname == name:
            sequence = [join(dir,x) for x in os.listdir(dir) if re.match(matchname, x)]
            return sequence

###### UPDATE FILES #############
    def update_studio_files_for_users(self, users=None):
        # rigs, textures, caches
        pass