import sys, argparse, os
import hou


parser = argparse.ArgumentParser(description='Join many image files together')
parser.add_argument('-hp', '--hip',
                    help='HIP file')
parser.add_argument('-o', '--otl',
                    nargs='*',
                    help='OTL files')
args = parser.parse_args()

hip = args.hip.replace('\\','/')
otls = [x.replace('\\','/') for x in args.otl]
for otl in otls:
    if os.path.exists:
        hou.hda.installFile(otl)

hou.hipFile.load(hip,  suppress_save_prompt=True, ignore_load_warnings=True)

print hou.node('/').allSubChildren()