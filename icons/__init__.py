import os
root = os.path.dirname(__file__)
ico = {os.path.splitext(name)[0]: os.path.join(root, name).replace('\\','/') for name in os.listdir(root) if os.path.splitext(name)[-1] == '.png'}