import os, sys, shutil
import amg_config
# get ftp path
ftp_target = amg_config.conf['ftp_path']

# root = os.path.dirname(__file__)
root = os.path.dirname(r'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\make_client_package.py')

def join(*args):
    return '/'.join(args).replace('\\','/')
# collect files
folders = [
    [join(root, 'amg_client'), join(ftp_target, 'amg_client')]
]

files = [
    [join(root,  'amg_file_manager.py'), join(ftp_target,  'amg_file_manager.py')],
    [join(root,  'ftpsync.py'), join(ftp_target,  'ftpsync.py')]
]
# copy files
for folder in folders:
    shutil.copytree(folder[0], folder[1])
for file in files:
    shutil.copy2(file[0], file[1])