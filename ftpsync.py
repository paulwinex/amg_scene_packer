import os, ftplib
from ftplib import FTP
import amg_file_manager
reload(amg_file_manager)
import amg_config
reload(amg_config)
from amg.shotgun import amg_shoutgun as ams
from amg import api


class FTP_Clone(object):
    def __init__(self, host, user, password):
        self.host = host
        self.user = user
        self.password = password
        self.is_connected =False
        self.local_path = ''
        self.remote_path = ''
        self.__ftp_files = []
        self.__project_meta_files = []

    def login(self):
        try:
            self.ftp = FTP(self.host)
            self.ftp.login(self.user, self.password)
            self.is_connected = True
            return True
        except ftplib.error_perm:
            return False

    def logout(self):
        try:
            self.ftp.quit()
            self.ftp.close()
        except ftplib.error_temp:
            pass

    def sync(self):
        to_upload, to_download = self.check_sync()
        self.sync_files(to_upload, to_download)

    def check_sync(self):
        to_upload = self.check_to_upload()
        to_download = self.check_to_download()
        print to_upload, to_download
        return to_upload, to_download

    def check_to_upload(self):
        res = []
        project_dir = amg_config.conf.get('user_projects_path')
        if not project_dir:
            print 'Project path not found'
            return
        for path, dirs, files in os.walk(project_dir):
            for f in files:
                if os.path.splitext(f)[-1] == amg_file_manager.Meta.suffix:
                    meta = amg_file_manager.Meta(os.path.join(path,f).replace('\\','/'))
                    if not meta['owner'] == amg_file_manager.Meta.STUDIO and not meta['uploaded']:
                        res.append(meta)
        return res

    def check_to_download(self):
        self.ftp.cwd('/')
        self.__ftp_files = []
        self.__find_meta_to_download('/')
        result = []
        studio_files = []
        for file in self.__ftp_files:
            m = amg_file_manager.Meta(file, self.ftp)
            if m['owner'] == amg_file_manager.Meta.STUDIO:
                studio_files.append(m)

        for meta in studio_files:
            full = amg_config.conf.get('user_projects_path', '') + meta.file
            # check exists
            if os.path.exists(full):
                #check modify time
                if int(os.stat(full).st_mtime) < meta['modify_at']:
                    result.append(meta)
            else:
                result.append(meta)
        return result, self.__project_meta_files

    def __find_meta_to_download(self, path):
        try:
            self.ftp.cwd(path)
        except:
            return
        for entry in (path for path in self.ftp.nlst() if path not in ('.', '..')):
            if self.is_file(entry):
                if entry == ams.project_meta_file:
                    full = os.path.join(path, entry).replace('\\','/')
                    if not full in self.__project_meta_files:
                        self.__project_meta_files.append(full)
                if os.path.splitext(entry)[-1] == amg_file_manager.Meta.suffix :
                    self.__ftp_files.append(os.path.join(path, entry).replace('\\','/'))
            else:
                self.__find_meta_to_download(os.path.join(path, entry).replace('\\','/'))

    def sync_files(self, upload_files=None, download_files=None, project_meta=None):
        # download
        if download_files:
            for meta in download_files:
                self.message(meta.file)
                full_local_path = amg_config.conf.get('user_projects_path', '')  + meta.file
                # make folder
                trg_dir = os.path.dirname(full_local_path)
                if not os.path.exists(trg_dir):
                    os.makedirs(trg_dir)
                # start download
                f = FTPWorker(self.ftp, meta.file, full_local_path)#, self.message)
                f.download()
                # save meta file
                meta['opened'] = True
                meta.save()
                # meta.is_ftp = False
                local_meta = amg_file_manager.Meta.create_for_local_file(full_local_path)
                local_meta.copy(meta)
                local_meta.save()
                # meta.copy_to_ftp( os.path.dirname(meta.file), self.ftp)
                # meta.copy_local_file(amg_config.conf.get('local_path', '') )
            # download project meta
            if project_meta:
                projects = list(set([x.file.strip('/').split('/')[0] for x in download_files]))
                for m in project_meta:
                    m_prj = m.strip('/').split('/')[0]
                    if m_prj in projects:
                        f = FTPWorker(self.ftp, m, os.path.join(amg_config.conf.get('user_projects_path', ''), m_prj, ams.project_meta_file))
                        f.download()
        # upload
        if upload_files:
            callback_data = dict(
                uploaded=[]
            )
            for meta in upload_files:
                self.message(meta.file)
                self.message(meta.file)
                local = amg_config.conf.get('user_projects_path', '')
                if not local:
                    raise Exception('Cant get local project path')
                full_ftp_path =  meta.file[len(local):]
                f = FTPWorker(self.ftp, full_ftp_path, meta.file)#, self.message)
                f.upload()
                #upload preview
                if meta.preview:
                    f = FTPWorker(self.ftp, meta.preview[len(local):], meta.preview)#, self.message)
                    f.upload()
                # meta
                meta['uploaded'] = True
                meta['opened'] = False
                meta['uploaded_at'] = amg_file_manager.timestamp()
                meta['modify_at'] = int(os.stat(meta.file).st_mtime)
                meta['owner'] = self.user
                meta.save()
                meta.copy_to_ftp(os.path.dirname(full_ftp_path), self.ftp)
                callback_data['uploaded'].append(full_ftp_path)
            self.upload_callback(callback_data)


    def message(self, msg):
        print msg

    def is_file(self, path):
        cur = path
        try:
            self.ftp.cwd(path)
            self.ftp.cwd(cur)
            return 0
        except ftplib.error_perm:
            try:
                sz = self.ftp.size(path)
                return sz
            except  ftplib.error_perm:
                return False

    def upload_callback(self, data):
        serv = api.server()
        r = serv.upload_signal()
        print 'RESPONSE: %s' % r


class FTPWorker(object):
    block_size = 1024*200
    def __init__(self, ftp, ftp_path, local_path, percent_callback=None):
        super(FTPWorker, self).__init__()
        self.ftp = ftp
        self.ftp_path = ftp_path
        self.local_path = local_path
        self.callback = percent_callback
        self.p = 0
        self.__outfile = None
        self.filesize = 1.0
        self.__i = 0.0

    def upload(self):
        self.filesize = os.path.getsize(self.local_path)
        self.__makedirs(os.path.dirname(self.ftp_path))
        self.currentfile = open(self.local_path,'rb')
        self.ftp.storbinary('STOR %s' % self.ftp_path, self.currentfile, self.block_size, self.__upload_callback)
        self.currentfile.close()
        if self.callback:
            self.callback(100)

    def __upload_callback(self, d):
        if self.callback:
            u = int(((self.block_size  * self.__i)/self.filesize)*100)
            if not self.p == u:
                self.p = u
                self.callback(u)
            self.__i += 1

    def download(self):
        self.filesize = float(self.ftp.size(self.ftp_path))
        self.__outfile = open(self.local_path, 'wb')
        self.ftp.retrbinary('RETR %s' % self.ftp_path, self.__download_callback)
        self.__outfile.close()
        if self.callback:
            self.callback(100)

    def __download_callback(self, data):
        if self.callback:
            n = int((self.__outfile.tell() / self.filesize)*100)
            if not n == self.p:
                self.callback(n)
                self.p = n
        self.__outfile.write(data)

    def __makedirs(self, path):
        parts = path.replace('\\','/').strip('/').split('/')
        cur = self.ftp.pwd()
        self.ftp.cwd('/')
        while parts:
            part = parts.pop(0)
            listdir = self.ftp.nlst()
            if not part in listdir:
                new = self.ftp.mkd(part)
            else:
                new = part
            self.ftp.cwd(new)
        self.ftp.cwd(cur)




# ftp=FTP("amg.com")
# ftp.login("guest","guest")
#
# # read text
#
# from StringIO import StringIO
# r = StringIO()
# ftp.retrbinary('RETR settings.json', r.write)
# r.seek(0)
# data = json.loads(r.getvalue())
#
# data['token'] = '123123'
# # write text
# import io
# ftp.cwd('/')
# bio = io.BytesIO(json.dumps(data, indent=2))
# ftp.storbinary('STOR settings.json', bio)
#
# ftp.nlst()
# ftp.quit()
# ftp.close()