# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\widgets\amg_tree_widget.ui'
#
# Created: Fri May 13 16:29:37 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_tree_widget(object):
    def setupUi(self, tree_widget):
        tree_widget.setObjectName("tree_widget")
        tree_widget.resize(697, 487)
        self.verticalLayout_3 = QtGui.QVBoxLayout(tree_widget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.full_rb = QtGui.QRadioButton(tree_widget)
        self.full_rb.setChecked(True)
        self.full_rb.setObjectName("full_rb")
        self.horizontalLayout.addWidget(self.full_rb)
        self.short_lb = QtGui.QRadioButton(tree_widget)
        self.short_lb.setObjectName("short_lb")
        self.horizontalLayout.addWidget(self.short_lb)
        self.filter_ly = QtGui.QHBoxLayout()
        self.filter_ly.setObjectName("filter_ly")
        self.horizontalLayout.addLayout(self.filter_ly)
        self.search_ly = QtGui.QHBoxLayout()
        self.search_ly.setObjectName("search_ly")
        self.horizontalLayout.addLayout(self.search_ly)
        self.clear_btn = QtGui.QPushButton(tree_widget)
        self.clear_btn.setMaximumSize(QtCore.QSize(30, 16777215))
        self.clear_btn.setObjectName("clear_btn")
        self.horizontalLayout.addWidget(self.clear_btn)
        self.verticalLayout_3.addLayout(self.horizontalLayout)
        self.tree_widget_wd = QtGui.QWidget(tree_widget)
        self.tree_widget_wd.setObjectName("tree_widget_wd")
        self.tree_ly = QtGui.QVBoxLayout(self.tree_widget_wd)
        self.tree_ly.setSpacing(3)
        self.tree_ly.setContentsMargins(0, 0, 0, 0)
        self.tree_ly.setContentsMargins(0, 0, 0, 0)
        self.tree_ly.setObjectName("tree_ly")
        self.verticalLayout_3.addWidget(self.tree_widget_wd)
        self.list_widget_wd = QtGui.QWidget(tree_widget)
        self.list_widget_wd.setObjectName("list_widget_wd")
        self.list_ly = QtGui.QVBoxLayout(self.list_widget_wd)
        self.list_ly.setContentsMargins(0, 0, 0, 0)
        self.list_ly.setContentsMargins(0, 0, 0, 0)
        self.list_ly.setObjectName("list_ly")
        self.verticalLayout_3.addWidget(self.list_widget_wd)

        self.retranslateUi(tree_widget)
        QtCore.QMetaObject.connectSlotsByName(tree_widget)

    def retranslateUi(self, tree_widget):
        tree_widget.setWindowTitle(QtGui.QApplication.translate("tree_widget", "Form", None, QtGui.QApplication.UnicodeUTF8))
        self.full_rb.setText(QtGui.QApplication.translate("tree_widget", "Full", None, QtGui.QApplication.UnicodeUTF8))
        self.short_lb.setText(QtGui.QApplication.translate("tree_widget", "Short", None, QtGui.QApplication.UnicodeUTF8))
        self.clear_btn.setText(QtGui.QApplication.translate("tree_widget", "X", None, QtGui.QApplication.UnicodeUTF8))

