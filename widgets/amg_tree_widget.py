from PySide.QtCore import *
from PySide.QtGui import *
try:
    from .. import amg_client
except:
    import amg_client
import amg_tree_widget_UIs
reload(amg_tree_widget_UIs)
import os, glob, re


class AMGTreeWidget(QWidget, amg_tree_widget_UIs.Ui_tree_widget):
    acceptSignal = Signal(object)
    selectSignal = Signal(object)
    MODE_FULL = 1
    MODE_SHORT = 2
    FILTER_NEW = 3
    FILTER_LAST = 4

    def __init__(self, multiselect=False, parent=None):
        super(AMGTreeWidget, self).__init__(parent)
        self.setupUi(self)
        self.tree = TreeWidgetClass(self)
        self.tree_ly.addWidget(self.tree)
        self.list = ListWidgetClass(self)
        self.list_ly.addWidget(self.list)
        self.search_le = SearchWidget(self)
        self.search_ly.addWidget(self.search_le)
        self.filter_widget = FilterWidget()
        self.filter_ly.addWidget(self.filter_widget)
        @self.filter_widget.updateFilterSignal.connect
        def _(f):
            self.filters = f
            self.filter_files()
        # config
        if multiselect:
            self.list.setSelectionMode(QAbstractItemView.ExtendedSelection)
            self.tree.setSelectionMode(QAbstractItemView.ExtendedSelection)
        else:
            self.list.setSelectionMode(QAbstractItemView.SingleSelection)
            self.tree.setSelectionMode(QAbstractItemView.SingleSelection)
        self.setTabOrder(self.search_le, self.list)
        self.tree.selectSignal.connect(self.selectSignal.emit)
        self.list.selectSignal.connect(self.selectSignal.emit)
        self.tree.acceptSignal.connect(self.acceptSignal.emit)
        self.list.acceptSignal.connect(self.acceptSignal.emit)

        # variables
        self.path = None
        self.filters = None
        self.client = amg_client.AMGOutsourceClient()

        self.filter_widget.update_filters()

        # connect
        self.search_le.textChanged.connect(self.start_search)
        self.search_le.goToListSignal.connect(self.go_to_list)
        self.clear_btn.clicked.connect(self.search_le.clear)
        self.full_rb.toggled.connect(lambda : self.fill(self.path, self.filter))
        # self.new_cbx.clicked.connect(self.filter_files)

        # start
        self.switch_tree_list()

    def fill(self, path, filter=None):
        if not path:
            return
        self.path = path
        self.filter = filter
        files = self.client.get_files(path, filter)
        if not files:
            print 'Empty files'
            return
        # save selected
        # files = self.filter_files(files)
        self.tree.fill(files, self.current_mode())
        self.list.fill(files, self.current_mode())
        # restore selected
        return True

    def filter_files(self):
        # filter new
        self.tree.apply_filter(self.filters)
        self.list.apply_filter(self.filters)


    def current_mode(self):
        if self.short_lb.isChecked():
            return self.MODE_SHORT
        else:
            return self.MODE_FULL

    # def accept(self):
    #     pass

    def get_data(self):
        if self.search_le.text():
            return self.list.get_data()
        else:
            return self.tree.get_data()

    def switch_tree_list(self):
        v = bool(self.search_le.text())
        self.list_widget_wd.setVisible(v)
        self.tree_widget_wd.setVisible(not v)

    def start_search(self, text):
        for f in self.filters:
            if f.name == _filter_search.name:
                f.text = text
                break
        self.switch_tree_list()
        self.list.apply_filter(self.filters)

    def go_to_list(self):
        if self.list.count():
            self.list.setFocus()
            self.list.setCurrentRow(0)

    def all_selected_data(self):
        if self.search_le.text():
            data_list = [x.data(32) for x in self.list.selectedItems()]
        else:
            sel = self.tree.selectedItems()
            data_list = []
            for it in sel:
                data_list += [x for x in self.__recursive_get_data(it) if not x in data_list]
        return list(set(data_list))

    def __recursive_get_data(self, item):
        files = []
        item_data = item.data(0,32)
        if item_data:
            files.append(item_data)
            return files
        for i in range(item.childCount()):
            ch = item.child(i)
            d = ch.data(0, 32)
            if d:
                files.append(d)
            else:
                files += self.__recursive_get_data(ch)
        return files

class TreeWidgetClass(QTreeWidget):
    acceptSignal = Signal(object)
    selectSignal = Signal(object)
    def __init__(self, parent):
        super(TreeWidgetClass, self).__init__(parent)

        self.header().hide()
        self.setVerticalScrollMode(QAbstractItemView.ScrollPerPixel)
        palette = self.palette()
        palette.setColor(QPalette.Highlight, QColor(70,80,90))
        self.setPalette(palette)
        self.sz = 24
        self.setIconSize(QSize(self.sz,self.sz))
        if not __name__ == '__main__':
            self.setStyleSheet("QTreeWidget::item{ height: "+str(self.sz)+"px;}")
        self.itemDoubleClicked.connect(self.double_cliched)
        self.expanded.connect(self.expand_all_recursive)
        self.collapsed.connect(self.expand_all_recursive)
        self.itemClicked.connect(self.__update_info)
        self.__data = None

    def fill(self, data, mode=AMGTreeWidget.MODE_FULL):
        self.clear()
        if mode == AMGTreeWidget.MODE_FULL:
            data = self.__compile_full_mode(data)
        elif mode == AMGTreeWidget.MODE_SHORT:
            data = self.__compile_short_mode(data)
        else:
            return
        if not data:
            return
        self.__add_elements(data)

    def __add_elements(self, array, parent=None):
        parent = parent or self.invisibleRootItem()
        for key, value in sorted(array.items()):
            item = QTreeWidgetItem(parent)
            item.setText(0, key)
            if isinstance(value, dict):
                self.__add_elements(value, item)
            else:
                item.setData(0, 32, value)
                # todo: add other information

    def __compile_full_mode(self, data):
        tree_files = {}
        def add_to_tree(elem, parts=None, parent=None):
            parent = parent if not parent is None else self.tree
            elems = parts or [elem.project.name] + elem.in_project_path().split('/')
            first = elems.pop(0)
            if first in parent:
                if not elems:
                    parent[first] = elem
                else:
                    add_to_tree(elem, elems, parent[first])
            else:
                if elems:
                    parent[first] = {}
                    add_to_tree(elem, elems, parent[first])
                else:
                    parent[first] = elem
        for f in data:
            add_to_tree(f, parent = tree_files)
        return tree_files

    def __compile_short_mode(self, data):
        tree_files = {}
        def add_to_tree(elem, parts=None, parent=None):
            parent = parent if not parent is None else self.tree
            elems = parts or elem.elements()[:]
            first = elems.pop(0)
            if first in parent:
                if not elems:
                    parent[first] = elem
                else:
                    add_to_tree(elem, elems, parent[first])
            else:
                if elems:
                    parent[first] = {}
                    add_to_tree(elem, elems, parent[first])
                else:
                    parent[first] = elem
        for f in data:
            add_to_tree(f, parent = tree_files)
        return tree_files


    def __add_element(self, element, branches, parent=None):
        parent = parent or self.invisibleRootItem()
        b = branches.pop(0)
        for i in range(parent.childCount()):
            ch = parent.child(i)
            if ch.text(0) == b:
                if branches:
                    self.__add_element(element, branches, ch)
                    break
        else:
            item = QTreeWidgetItem(parent)
            item.setText(0, b)
            if branches:
                self.__add_element(element, branches, item)
            else:
                item.setData(0, 32, element)

    def __update_info(self, item):
        data = item.data(0,32)
        if data:
            self.__data = data
        else:
            self.__data = None
        self.selectSignal.emit(self.__data)

    def expand_all_recursive(self, item):
        item = self.itemFromIndex(item)
        if QApplication.keyboardModifiers() == Qt.ShiftModifier:
            def expand_all(it, expand=True):
                for i in range(it.childCount()):
                    el = it.child(i)
                    el.setExpanded(expand)
                    expand_all(el, expand)
            self.blockSignals(1)
            expand_all(item, item.isExpanded())
            self.blockSignals(0)

    def double_cliched(self, item):
        d = item.data(0, 32)
        if d:
            self.__data = d
            self.acceptSignal.emit(self.__data)

    def apply_filter(self, filters):
        filters = [x for x in filters if not x.name == _filter_search.name]
        iterator = QTreeWidgetItemIterator(self)
        while iterator.value():
            item = iterator.value()
            if not all(map(lambda f, i=item:f.check(i), filters )):
                item.setHidden(True)
            else:
                item.setHidden(False)
            self.hide_empty(item)
            iterator += 1


    def hide_empty(self, item):
        p = item.parent()
        if p:
            if not len([p.child(x) for x in range(p.childCount()) if not p.child(x).isHidden()]):
                p.setHidden(True)
            else:
                p.setHidden(False)
            self.hide_empty(p)

    def get_data(self):
        return self.__data

    def custom_menu(self, item, pos):
        menu = QMenu(self.parent())
        menu.addAction(QAction('Explore', self, triggered=lambda x=item: open_folder(x.data(0, 32).meta.file)) )
        menu.exec_(pos)

    def mousePressEvent(self, event):
        if event.button() == Qt.RightButton:
            # pos = event.globalPos()
            item = self.itemAt(event.pos())
            if item:
                self.custom_menu(item, event.globalPos())
                return
        super(TreeWidgetClass, self).mousePressEvent(event)


class ListWidgetClass(QListWidget):
    acceptSignal = Signal(object)
    selectSignal = Signal(object)
    def __init__(self, parent):
        super(ListWidgetClass, self).__init__(parent)
        self.sz = 24
        self.setIconSize(QSize(self.sz,self.sz))
        if not __name__ == '__main__':
            self.setStyleSheet("QListWidget::item{ height: "+str(self.sz)+"px;}")
        self.itemDoubleClicked.connect(self.accept_action)
        @self.itemSelectionChanged.connect
        def show_selected():
            self.update_info(self.currentItem())
        self.__data = None

    def fill(self, data, mode=AMGTreeWidget.MODE_FULL):
        self.clear()
        if mode == AMGTreeWidget.MODE_FULL:
            data = self.__compile_full_mode(data)
        elif mode == AMGTreeWidget.MODE_SHORT:
            data = self.__compile_short_mode(data)
        else:
            return
        if not data:
            return
        for key, value in data.items():
            item = QListWidgetItem(self)
            item.setText(key)
            item.setData(32, value)
        self.sortItems(Qt.AscendingOrder)


    def __compile_full_mode(self, data):
        return {'/'.join([elem.project.name, elem.in_project_path()]): elem for elem in data}

    def __compile_short_mode(self, data):
        return {('/'.join(elem.elements()) ):elem for elem in data}

    def filter(self, text):
        showAll = False
        if not text:
            showAll = True
        for i in range(self.count()):
            item = self.item(i)
            if showAll:
                item.setHidden(False)
            else:
                item.setHidden(not text.lower() in item.text().lower())

    def get_data(self):
        return self.__data

    def accept_action(self):
        sel = self.selectedItems()
        if sel:
            self.__data = sel[0].data(32)
            self.acceptSignal.emit(self.__data)

    def update_info(self, item):
        data = item.data(32)
        if data:
            self.__data = data
        else:
            self.__data = None
        self.selectSignal.emit(self.__data)

    def apply_filter(self, filters):
        for i in range(self.count()):
            item = self.item(i)
            if not all(map(lambda f, i=item:f.check(i), filters )):
                item.setHidden(True)
            else:
                item.setHidden(False)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Return or event.key() == Qt.Key_Enter:
            self.accept_action()
        super(ListWidgetClass, self).keyPressEvent(event)




class SearchWidget(QLineEdit):
    goToListSignal = Signal()
    def __init__(self, parent):
        super(SearchWidget, self).__init__()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Down:
            self.goToListSignal.emit()
        super(SearchWidget, self).keyPressEvent(event)


############################### FILTER

class FilterWidget(QLabel):
    updateFilterSignal = Signal(list)
    def __init__(self):
        super(FilterWidget, self).__init__()
        self.setText('Filter')
        self.filters = []
        for name, cls in globals().items():
            if name.startswith('_filter'):
                self.filters.append(cls())
        self.menu = QMenu()
        cla = QAction('Clear All', self.menu, triggered=self.clear_all)
        self.menu.addAction(cla)
        self.menu.addSeparator()
        for c in [x for x in self.filters if x.menu]:
            a = QAction(c.title, self.menu)
            a.triggered.connect(lambda fil=c, act=a: self.switch_action(fil, act))
            a.setCheckable(True)
            a.setData(c)
            self.menu.addAction(a)

    def switch_action(self, cls, action):
        cls.on = action.isChecked()
        self.update_filters()

    def update_filters(self):
        # filters = [a.data() for a in self.menu.actions() if a.isChecked()]
        filters = [f for f in self.filters if not f.menu or f.on]
        self.updateFilterSignal.emit(filters)

    def clear_all(self):
        self.blockSignals(1)
        for a in self.menu.actions():
            a.setChecked(False)
        self.blockSignals(0)
        for f in [x for x in self.filters if x.menu]:
            f.on = False
        self.update_filters()

    def mousePressEvent(self, event):
        self.menu.exec_(QCursor.pos())
        super(FilterWidget, self).mousePressEvent(event)

class __filter(object):
    title=''
    name = ''
    menu=True
    def __init__(self):
        self.on=False
    def check(self, item):
        return True
    def __repr__(self):
        return '<Filter> %s' % self.title

class _filter_new(__filter):
    name = 'new'
    title = 'New Only'
    def check(self, item):
        if isinstance(item, QListWidgetItem):
            return item.data(32).meta['opened'] is False
        else:
            d = item.data(0,32)
            if d:
                # print d
                # print d.meta
                return d.meta['opened'] is False
            else:
                return True

class _filter_last(__filter):
    name = 'last'
    title = 'Last Version'
    def check(self, item):
        if isinstance(item, QListWidgetItem):
            f = item.data(32)
        else:
            f = item.data(0,32)
        if not f:
            return True
        path = f.meta.file
        return os.path.basename(path) == sorted(glob.glob1(os.path.dirname(path), re.sub(r"v\d+", "v*", os.path.basename(path))))[-1]

class _filter_search(__filter):
    name = 'search'
    title = 'Search'
    menu=False
    def __init__(self):
        super(_filter_search, self).__init__()
        self.text = ''
    def check(self, item):
        if isinstance(item, QListWidgetItem):
            return all([t.lower() in item.text().lower() for t in self.text.split()])
        else:
            return all([t.lower() in item.text(0).lower() for t in self.text.split()])

def open_folder( path):
    if os.name == 'nt':
        if os.path.isfile(path):
            cmd = 'explorer.exe /n, /e, /select, %s' % path.replace('/','\\')
            p = QProcess()
            p.startDetached(cmd)
        else:
            os.startfile(path)
    elif os.name == 'posix':
        os.system('xdg-open "%s"' % path.replace('\\','/'))
    elif os.name == 'os2':
        os.system('open "%s"' % path.replace('\\','/'))


if __name__ == '__main__':
    app = QApplication([])
    w = AMGTreeWidget(multiselect=True)
    w.show()
    app.exec_()
