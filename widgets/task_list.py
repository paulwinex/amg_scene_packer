from PySide.QtCore import *
from PySide.QtGui import *

class AMGtaskList(QTextBrowser):
    def __init__(self):
        super(AMGtaskList, self).__init__()
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.custom_menu)


    def message(self, msg):
        self.append('%s' % msg)
        self.repaint()

    def custom_menu(self):
        menu = QMenu(self)
        menu.addAction(QAction('Clear', self, triggered=self.clear))
        menu.exec_(QCursor.pos())