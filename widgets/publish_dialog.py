# -*- coding: utf-8 -*-
from PySide.QtCore import *
from PySide.QtGui import *
import publish_dialog_UIs
reload(publish_dialog_UIs)
import os, datetime
from .. import amg_file_manager
import amg_config

# QTextCodec.setCodecForTr(QTextCodec.codecForName("utf8"))
# QTextCodec.setCodecForLocale(QTextCodec.codecForName("utf8"))

class PublishDialog(QMainWindow, publish_dialog_UIs.Ui_publish_dialog):
    closeSignal = Signal(bool)
    def __init__(self, filename, send_callback, parent=None):
        super(PublishDialog, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(Qt.Tool)
        self.meta = amg_file_manager.Meta.create_for_local_file(filename)
        self.meta['artist'] = amg_config.conf['username']
        self.preview_img = None
        self.can_publish = True
        self.send_callback = send_callback

        self.setWindowTitle('Publish %s' % os.path.basename(self.meta.file))
        self.preview_lb = PreviewLabel(self)
        self.preview_ly.addWidget(self.preview_lb)
        self.publish_btn.clicked.connect(self.publish)

        if parent:
            center = parent.geometry().center()
            rec = self.geometry()
            rec.moveCenter(center)
            self.setGeometry(rec)
        self.check_can_publish()
        self.update_info()
        if self.meta.preview:
            self.preview_lb.init_image(self.meta.preview)

    def publish(self):
        # comment = self.comment_te.toPlainText()
        self.preview_img = self.preview_lb.pix
        self.send_callback(self.meta, self.comment_te.toPlainText(), self.preview_img)
        self.close()
        self.closeSignal.emit(self.upload_cbx.isChecked())

    def disable_all(self):
        self.setDisabled(1)

    def check_can_publish(self):
        if self.meta['owner'] == amg_file_manager.Meta.STUDIO:
            info = 'This is studio file'
            self.disable_all()
            self.error_message(info)
            self.can_publish = False
            return

        if os.path.exists(self.meta.path):
            info = 'File already published\nYou will rewrite meta file'
            # self.disable_all()
            self.warning_message(info)
            return

        localpath = amg_config.conf.get('user_projects_path')
        if not localpath:
            info = 'Value "user_projects_path" not found in config'
            self.disable_all()
            self.error_message(info)
            self.can_publish = False
            return

        if not self.meta.file.startswith(localpath):
            info = 'Scene not in projects directory\n%s' % self.meta.file
            self.disable_all()
            self.error_message(info)
            self.can_publish = False
            return

    def error_message(self, msg):
        QMessageBox.critical(self, 'Error', msg, QMessageBox.Ok)

    def warning_message(self, msg):
        QMessageBox.warning(self, 'Warning', msg, QMessageBox.Ok)

    def update_info(self):
        if self.meta.file:
            dt = int(os.stat(self.meta.file).st_mtime)
            time = datetime.datetime.fromtimestamp(dt).strftime('%Y.%m.%d %H:%M:%S')
            info = '''Name: {name}
Save Date: {date}
'''.format(name=os.path.basename(self.meta.file), date = time)
        else:
            info = 'File not exists'
        self.info_lb.setText(info)


class PreviewLabel(QLabel):
    def __init__(self, parent):
        super(PreviewLabel, self).__init__(parent)
        self.dial = parent
        self.setFixedSize(QSize(amg_config.conf.get('thumbnails_size',[300])[0], amg_config.conf.get('thumbnails_size',[0,300])[1]))
        self.setAlignment(Qt.AlignCenter)
        self.setText('Click to create preview')
        self.setMouseTracking(1)
        self.pix = None

    def enterEvent(self, event):
        self.setStyleSheet('background-color:black;color:white;')
        super(PreviewLabel, self).enterEvent(event)

    def leaveEvent(self, event):
        self.setStyleSheet('')
        super(PreviewLabel, self).leaveEvent(event)

    def mouseReleaseEvent(self, event):
        super(PreviewLabel, self).mouseReleaseEvent(event)
        self.dial.hide()
        QTimer.singleShot(100,self.get_image)

    def get_image(self):
        self.grabber = captureRegionClass()
        self.grabber.endSelectionSignal.connect(self.end_grab)
        self.grabber.show()

    def end_grab(self, pix):
        if not pix.isNull():
            if pix.size().height() < 50 or pix.size().width() < 50:
                self.setText('Image too small. Please try again')
            else:
                self.pix = self.rescale(pix)
                self.setPixmap(self.pix)
        self.dial.show()

    def init_image(self, path):
        if os.path.exists(path):
            pix = QPixmap(path)
            if not pix.isNull():
                self.pix = self.rescale(pix)
                self.setPixmap(self.pix)

    def rescale(self, pix):
        h = pix.height()
        w = pix.width()
        if not h == w:
            if h > w:
                crop_box = [0, (h-w)/2, w, w]
                pix = pix.copy(QRect(*crop_box))
            elif w > h:
                crop_box = [(w-h)/2, 0, h, h]
                pix = pix.copy(QRect(*crop_box))
        pix = pix.scaled(amg_config.conf.get('thumbnails_size',[300])[0], amg_config.conf.get('thumbnails_size',[0,300])[1],
                     Qt.KeepAspectRatio, Qt.SmoothTransformation)
        return pix


class captureRegionClass(QWidget):
    endSelectionSignal = Signal(QPixmap)
    regionColor = QColor(200,200,200)
    textColor = QColor(0,0,0)
    bgColor = QColor(200,200,200)
    def __init__(self):
        super(captureRegionClass, self).__init__()
        self.setWindowFlags( Qt.WindowStaysOnTopHint | Qt.FramelessWindowHint)
        scr = QApplication.desktop()
        w = scr.winId()
        screens = scr.screenCount()
        mx = 0
        my = 0
        for i in range(screens):
            screenRect = scr.screen(i).geometry()
            mx = min(mx, screenRect.x())
            my = min(my, screenRect.y())
        rec = QRect(mx,my,scr.width(), scr.height())
        # rec = QRect(0,0,scr.width(), scr.height())
        # self.pix = QPixmap.grabWindow(w,0,0,scr.width(), scr.height())
        self.pix = QPixmap.grabWindow(w,mx,my,scr.width(), scr.height())
        self.setGeometry(rec)
        self.startPos = None
        self.endPos= None

    def showEvent(self, event):
        QApplication.setOverrideCursor(QCursor(Qt.CrossCursor))
        super(captureRegionClass, self).showEvent(event)

    def paintEvent(self, event):
        super(captureRegionClass, self).paintEvent(event)
        painter = QPainter()
        painter.begin(self)
        painter.drawPixmap(0, 0, self.pix)
        if self.startPos and self.endPos:
            # draw frame
            painter.setCompositionMode(QPainter.RasterOp_SourceXorDestination)
            painter.setPen(QPen( QBrush(self.regionColor), 1 ))
            rec = QRect(self.startPos, self.endPos).normalized ()
            painter.drawRect(rec)
            # text bg
            bgPos1 = rec.topLeft() + QPoint(0, -5)
            bgPos2 = bgPos1 + QPoint(60, -18)
            painter.setCompositionMode(QPainter.CompositionMode_SourceOver)
            bgRec = QRect(bgPos1, bgPos2)
            brush = QBrush(self.bgColor)
            painter.fillRect(bgRec, brush)
            # text
            textPos = rec.topLeft() - QPoint(-4, 10)
            painter.setPen(QPen( QBrush(self.textColor), 1 ))
            painter.drawText(textPos, '%s x %s' % (rec.width(), rec.height()))
        painter.end()

    def mousePressEvent(self, event):
        if event.button() == Qt.LeftButton:
            self.startPos = event.pos()
            self.endPos = event.pos()
        super(captureRegionClass, self).mousePressEvent(event)

    def mouseReleaseEvent(self, event):
        if event.button() == Qt.LeftButton:
            QApplication.restoreOverrideCursor()
            self.sendImage()
        super(captureRegionClass, self).mouseReleaseEvent(event)

    def mouseMoveEvent(self, event):
        self.endPos = event.pos()
        self.repaint()
        super(captureRegionClass, self).mouseMoveEvent(event)

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Escape:
            QApplication.restoreOverrideCursor()
            self.endSelectionSignal.emit(QPixmap)
            self.hide()


    def cutRegion(self, region):
        path = QFileDialog.getSaveFileName(self,'Save file to...', '', ("Images JPG (*.jpg)"))
        if path[0]:
            cut = self.pix.copy(region)
            cut.save(path[0], 'JPG', 90)
        self.hide()

    def getRegion(self):
        if self.region:
            cut = self.pix.copy(self.region)
            return cut

    def saveRegion(self):
        self.region = QRect(self.startPos, self.endPos).normalized()

    def sendImage(self):
        self.hide()
        region = QRect(self.startPos, self.endPos).normalized()
        cut = self.pix.copy(region)
        self.endSelectionSignal.emit(cut)
        self.pix = None


def justSave(pix):
    pix.save('c:/image.jpg', 'JPG', 90)

if __name__ == '__main__':
    app = QApplication([])
    grabber = captureRegionClass()
    grabber.endSelectionSignal.connect(justSave)
    grabber.show()
    app.exec_()