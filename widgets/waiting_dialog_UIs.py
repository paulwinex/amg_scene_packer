# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\widgets\waiting_dialog.ui'
#
# Created: Wed May 11 16:31:57 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_waiting_dialod(object):
    def setupUi(self, waiting_dialod):
        waiting_dialod.setObjectName("waiting_dialod")
        waiting_dialod.resize(490, 126)
        self.horizontalLayout_2 = QtGui.QHBoxLayout(waiting_dialod)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.icon_lb = QtGui.QLabel(waiting_dialod)
        self.icon_lb.setObjectName("icon_lb")
        self.horizontalLayout.addWidget(self.icon_lb)
        self.message_lb = QtGui.QLabel(waiting_dialod)
        font = QtGui.QFont()
        font.setPointSize(12)
        self.message_lb.setFont(font)
        self.message_lb.setObjectName("message_lb")
        self.horizontalLayout.addWidget(self.message_lb)
        self.horizontalLayout_2.addLayout(self.horizontalLayout)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)

        self.retranslateUi(waiting_dialod)
        QtCore.QMetaObject.connectSlotsByName(waiting_dialod)

    def retranslateUi(self, waiting_dialod):
        waiting_dialod.setWindowTitle(QtGui.QApplication.translate("waiting_dialod", "Waiting...", None, QtGui.QApplication.UnicodeUTF8))
        self.icon_lb.setText(QtGui.QApplication.translate("waiting_dialod", "ICON", None, QtGui.QApplication.UnicodeUTF8))
        self.message_lb.setText(QtGui.QApplication.translate("waiting_dialod", "Connect to Shotgun server...", None, QtGui.QApplication.UnicodeUTF8))

