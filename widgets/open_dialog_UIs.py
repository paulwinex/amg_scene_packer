# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\widgets\open_dialog.ui'
#
# Created: Fri May 13 11:50:45 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_open_scene(object):
    def setupUi(self, open_scene):
        open_scene.setObjectName("open_scene")
        open_scene.resize(1061, 578)
        self.horizontalLayout_2 = QtGui.QHBoxLayout(open_scene)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout_4 = QtGui.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.tree_ly = QtGui.QVBoxLayout()
        self.tree_ly.setObjectName("tree_ly")
        self.verticalLayout_4.addLayout(self.tree_ly)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.cancel_btn = QtGui.QPushButton(open_scene)
        self.cancel_btn.setObjectName("cancel_btn")
        self.horizontalLayout.addWidget(self.cancel_btn)
        self.open_btn = QtGui.QPushButton(open_scene)
        self.open_btn.setObjectName("open_btn")
        self.horizontalLayout.addWidget(self.open_btn)
        self.verticalLayout_4.addLayout(self.horizontalLayout)
        self.horizontalLayout_2.addLayout(self.verticalLayout_4)
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.preview_lb = QtGui.QLabel(open_scene)
        self.preview_lb.setMinimumSize(QtCore.QSize(250, 250))
        self.preview_lb.setMaximumSize(QtCore.QSize(250, 250))
        self.preview_lb.setAlignment(QtCore.Qt.AlignCenter)
        self.preview_lb.setObjectName("preview_lb")
        self.verticalLayout.addWidget(self.preview_lb)
        self.infi_tbw = QtGui.QTextBrowser(open_scene)
        self.infi_tbw.setObjectName("infi_tbw")
        self.verticalLayout.addWidget(self.infi_tbw)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        self.horizontalLayout_2.setStretch(0, 1)

        self.retranslateUi(open_scene)
        QtCore.QMetaObject.connectSlotsByName(open_scene)

    def retranslateUi(self, open_scene):
        open_scene.setWindowTitle(QtGui.QApplication.translate("open_scene", "Open Scene", None, QtGui.QApplication.UnicodeUTF8))
        self.cancel_btn.setText(QtGui.QApplication.translate("open_scene", "Cancel", None, QtGui.QApplication.UnicodeUTF8))
        self.open_btn.setText(QtGui.QApplication.translate("open_scene", "Open", None, QtGui.QApplication.UnicodeUTF8))
        self.preview_lb.setText(QtGui.QApplication.translate("open_scene", "Preview", None, QtGui.QApplication.UnicodeUTF8))
        self.infi_tbw.setHtml(QtGui.QApplication.translate("open_scene", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt; font-weight:600;\">File Info:</span></p>\n"
"<p style=\" margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:8pt;\">Name: filename.mb<br />Type: Maya FIle<br />Last Modify Date: 12.04.2016<br />Version: 3<br />Owner: user<br />Comment:<br />comment</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:8pt;\"><br /></p></body></html>", None, QtGui.QApplication.UnicodeUTF8))

