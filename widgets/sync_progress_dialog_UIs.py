# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\client\widgets\sync_progress_dialog.ui'
#
# Created: Tue May 03 20:24:02 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_sync_progress(object):
    def setupUi(self, sync_progress):
        sync_progress.setObjectName("sync_progress")
        sync_progress.resize(779, 419)
        self.centralwidget = QtGui.QWidget(sync_progress)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout = QtGui.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.progress_global_lb = QtGui.QLabel(self.centralwidget)
        self.progress_global_lb.setObjectName("progress_global_lb")
        self.verticalLayout.addWidget(self.progress_global_lb)
        self.progress_global_pb = QtGui.QProgressBar(self.centralwidget)
        self.progress_global_pb.setProperty("value", 0)
        self.progress_global_pb.setObjectName("progress_global_pb")
        self.verticalLayout.addWidget(self.progress_global_pb)
        self.progress_file_lb = QtGui.QLabel(self.centralwidget)
        self.progress_file_lb.setObjectName("progress_file_lb")
        self.verticalLayout.addWidget(self.progress_file_lb)
        self.progress_file_pb = QtGui.QProgressBar(self.centralwidget)
        self.progress_file_pb.setProperty("value", 0)
        self.progress_file_pb.setObjectName("progress_file_pb")
        self.verticalLayout.addWidget(self.progress_file_pb)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.icon_lb = QtGui.QLabel(self.centralwidget)
        self.icon_lb.setAlignment(QtCore.Qt.AlignCenter)
        self.icon_lb.setObjectName("icon_lb")
        self.verticalLayout_2.addWidget(self.icon_lb)
        self.stop_btn = QtGui.QPushButton(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.stop_btn.sizePolicy().hasHeightForWidth())
        self.stop_btn.setSizePolicy(sizePolicy)
        self.stop_btn.setObjectName("stop_btn")
        self.verticalLayout_2.addWidget(self.stop_btn)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        self.verticalLayout_3.addLayout(self.horizontalLayout_2)
        self.output_tb = QtGui.QTextBrowser(self.centralwidget)
        self.output_tb.setObjectName("output_tb")
        self.verticalLayout_3.addWidget(self.output_tb)
        sync_progress.setCentralWidget(self.centralwidget)

        self.retranslateUi(sync_progress)
        QtCore.QMetaObject.connectSlotsByName(sync_progress)

    def retranslateUi(self, sync_progress):
        sync_progress.setWindowTitle(QtGui.QApplication.translate("sync_progress", "AMG Synchronize Progress", None, QtGui.QApplication.UnicodeUTF8))
        self.progress_global_lb.setText(QtGui.QApplication.translate("sync_progress", "12 / 20", None, QtGui.QApplication.UnicodeUTF8))
        self.progress_file_lb.setText(QtGui.QApplication.translate("sync_progress", "filename", None, QtGui.QApplication.UnicodeUTF8))
        self.icon_lb.setText(QtGui.QApplication.translate("sync_progress", "animation", None, QtGui.QApplication.UnicodeUTF8))
        self.stop_btn.setText(QtGui.QApplication.translate("sync_progress", "Stop", None, QtGui.QApplication.UnicodeUTF8))

