# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\client\widgets\sync_files_dialog.ui'
#
# Created: Thu May 05 23:30:06 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_sync_dialog(object):
    def setupUi(self, sync_dialog):
        sync_dialog.setObjectName("sync_dialog")
        sync_dialog.resize(771, 678)
        self.verticalLayout_3 = QtGui.QVBoxLayout(sync_dialog)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label = QtGui.QLabel(sync_dialog)
        self.label.setObjectName("label")
        self.verticalLayout_3.addWidget(self.label)
        self.groupBox = QtGui.QGroupBox(sync_dialog)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.list_download_ly = QtGui.QVBoxLayout()
        self.list_download_ly.setObjectName("list_download_ly")
        self.verticalLayout.addLayout(self.list_download_ly)
        self.verticalLayout_3.addWidget(self.groupBox)
        self.groupBox_2 = QtGui.QGroupBox(sync_dialog)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.groupBox_2)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.list_upload_ly = QtGui.QVBoxLayout()
        self.list_upload_ly.setObjectName("list_upload_ly")
        self.verticalLayout_2.addLayout(self.list_upload_ly)
        self.verticalLayout_3.addWidget(self.groupBox_2)
        self.sync_btn = QtGui.QPushButton(sync_dialog)
        self.sync_btn.setMinimumSize(QtCore.QSize(0, 42))
        self.sync_btn.setObjectName("sync_btn")
        self.verticalLayout_3.addWidget(self.sync_btn)
        self.verticalLayout_3.setStretch(1, 1)
        self.verticalLayout_3.setStretch(2, 1)

        self.retranslateUi(sync_dialog)
        QtCore.QMetaObject.connectSlotsByName(sync_dialog)

    def retranslateUi(self, sync_dialog):
        sync_dialog.setWindowTitle(QtGui.QApplication.translate("sync_dialog", "AMG Synchronize Files", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("sync_dialog", "Use Spacebar to check / uncheck items", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("sync_dialog", "Download", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("sync_dialog", "Upload", None, QtGui.QApplication.UnicodeUTF8))
        self.sync_btn.setText(QtGui.QApplication.translate("sync_dialog", "SYNC", None, QtGui.QApplication.UnicodeUTF8))

