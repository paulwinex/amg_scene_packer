from PySide.QtCore import *
from PySide.QtGui import *


class OpenDialogSearch(QLineEdit):
    goToListSignal = Signal()
    def __init__(self):
        super(OpenDialogSearch, self).__init__()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Down:
            self.goToListSignal.emit()
        super(OpenDialogSearch, self).keyPressEvent(event)
