from PySide.QtCore import *
from PySide.QtGui import *


class AMGPackerList(QListWidget):
    messageSignal = Signal(str)
    def __init__(self):
        super(AMGPackerList, self).__init__()
