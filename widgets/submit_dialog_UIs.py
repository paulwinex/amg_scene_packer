# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\widgets\submit_dialog.ui'
#
# Created: Wed May 11 16:22:25 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_submit_files(object):
    def setupUi(self, submit_files):
        submit_files.setObjectName("submit_files")
        submit_files.resize(982, 443)
        self.horizontalLayout = QtGui.QHBoxLayout(submit_files)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.icon_download_lb = QtGui.QLabel(submit_files)
        self.icon_download_lb.setMinimumSize(QtCore.QSize(100, 100))
        font = QtGui.QFont()
        font.setPointSize(28)
        font.setWeight(75)
        font.setBold(True)
        self.icon_download_lb.setFont(font)
        self.icon_download_lb.setAlignment(QtCore.Qt.AlignCenter)
        self.icon_download_lb.setObjectName("icon_download_lb")
        self.horizontalLayout.addWidget(self.icon_download_lb)
        self.verticalLayout_2 = QtGui.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.groupBox = QtGui.QGroupBox(submit_files)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName("verticalLayout")
        self.files_ly = QtGui.QVBoxLayout()
        self.files_ly.setObjectName("files_ly")
        self.verticalLayout.addLayout(self.files_ly)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.create_version_cbx = QtGui.QCheckBox(submit_files)
        self.create_version_cbx.setObjectName("create_version_cbx")
        self.verticalLayout_2.addWidget(self.create_version_cbx)
        self.submit_btn = QtGui.QPushButton(submit_files)
        self.submit_btn.setObjectName("submit_btn")
        self.verticalLayout_2.addWidget(self.submit_btn)
        self.horizontalLayout.addLayout(self.verticalLayout_2)
        self.icon_upload_lb = QtGui.QLabel(submit_files)
        self.icon_upload_lb.setMinimumSize(QtCore.QSize(100, 100))
        font = QtGui.QFont()
        font.setPointSize(28)
        font.setWeight(75)
        font.setBold(True)
        self.icon_upload_lb.setFont(font)
        self.icon_upload_lb.setAlignment(QtCore.Qt.AlignCenter)
        self.icon_upload_lb.setObjectName("icon_upload_lb")
        self.horizontalLayout.addWidget(self.icon_upload_lb)

        self.retranslateUi(submit_files)
        QtCore.QMetaObject.connectSlotsByName(submit_files)

    def retranslateUi(self, submit_files):
        submit_files.setWindowTitle(QtGui.QApplication.translate("submit_files", "Submit Files", None, QtGui.QApplication.UnicodeUTF8))
        self.icon_download_lb.setText(QtGui.QApplication.translate("submit_files", "<<", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("submit_files", "File list", None, QtGui.QApplication.UnicodeUTF8))
        self.create_version_cbx.setText(QtGui.QApplication.translate("submit_files", "Create New Version in Shotgun", None, QtGui.QApplication.UnicodeUTF8))
        self.submit_btn.setText(QtGui.QApplication.translate("submit_files", "Submit", None, QtGui.QApplication.UnicodeUTF8))
        self.icon_upload_lb.setText(QtGui.QApplication.translate("submit_files", ">>", None, QtGui.QApplication.UnicodeUTF8))

