# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from PySide.QtCore import *
from PySide.QtGui import *
import os, re, datetime
import open_dialog_UIs
import amg_tree_widget
reload(amg_tree_widget)
reload(open_dialog_UIs)
from .. import amg_client, amg_file_manager
reload(amg_client)
reload(amg_file_manager)
import amg_config


class AMGOpenDialog(QDialog, open_dialog_UIs.Ui_open_scene):
    def __init__(self, parent, file_filter=None):
        super(AMGOpenDialog, self).__init__(parent)
        self.setupUi(self)
        self.tree = amg_tree_widget.AMGTreeWidget(multiselect=False)
        self.tree_ly.addWidget(self.tree)
        self.tree.acceptSignal.connect(self.accept)
        self.tree.selectSignal.connect(self.update_info)
        self.file_filter = file_filter
        self.client = amg_client.AMGOutsourceClient()
        self.data = None

        # connects
        self.open_btn.clicked.connect(self.accept)
        self.cancel_btn.clicked.connect(self.reject)

        # start
        self.fill_ui()
        self.update_info()
        self.tree.search_le.setFocus()

    def accept(self, data=None):
        self.data = data or self.tree.get_data()
        if self.data:
            super(AMGOpenDialog, self).accept()
        else:
            return

    def get_data(self):
        return self.data

    def __update_visibility(self):
        if self.search.text():
            self.mode = 1
            self.list.show()
            self.tree.hide()
        else:
            self.mode = 0
            self.list.hide()
            self.tree.show()

    def fill_ui(self):
        print amg_config.conf['user_projects_path']
        self.tree.fill(amg_config.conf['user_projects_path'], self.file_filter)

    def update_info(self, data=None):
        if data:
            self.infi_tbw.setHtml(info.format(
                filename=os.path.basename(data.path),
                type=amg_file_manager.types.get(os.path.splitext(data.path)[-1].strip('.'), amg_file_manager.types['other']),
                date=amg_file_manager.stamp_to_str(data.meta['modify_at']) if data.meta else '',
                version=(re.findall(r"v(\d+)", os.path.splitext(os.path.basename(data.path))[0]) or [''])[0],
                user=data.meta['owner'] if data.meta else '',
                comment=data.meta['comment'] if data.meta else ''
            ))
            meta = amg_client.amg_file_manager.Meta.create_for_local_file(data.path)
            if meta.preview:
                pix = QPixmap(meta.preview).scaled(250,250, Qt.KeepAspectRatio, Qt.SmoothTransformation)
                self.preview_lb.setPixmap(pix)
            else:
                self.preview_lb.setPixmap(QPixmap())
        else:
             self.infi_tbw.setHtml('<h2><b>Select file</b></h2>')
             self.preview_lb.setPixmap(QPixmap())


info = '''<html>
<body style="font-family:'MS Shell Dlg 2'; font-size:10pt; font-weight:400; font-style:normal;">
<h2>
    <b>File Info:</b>
</h2>
<p style="margin-top:5px; margin-bottom:5px; margin-left:10px; margin-right:0px; -qt-block-indent:0; text-indent:0px;">
    <br />Name: <b>{filename}</b>
    <br />Type: <b>{type}</b>
    <br />Modify Date: <b>{date}</b>
    <br />Version: <b>{version}</b>
    <br />Owner: <b>{user}</b>
    <br />Comment:
    <br />{comment}
</span></p>
</body></html>
'''

