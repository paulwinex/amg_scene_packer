# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'D:\work\amg\amg_pipeline\scripts\amg\utils\amg_scene_packer\client\widgets\publish_dialog.ui'
#
# Created: Tue May 10 22:49:28 2016
#      by: pyside-uic 0.2.15 running on PySide 1.2.4
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_publish_dialog(object):
    def setupUi(self, publish_dialog):
        publish_dialog.setObjectName("publish_dialog")
        publish_dialog.resize(357, 526)
        self.centralwidget = QtGui.QWidget(publish_dialog)
        self.centralwidget.setObjectName("centralwidget")
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.groupBox = QtGui.QGroupBox(self.centralwidget)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout = QtGui.QHBoxLayout(self.groupBox)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.preview_ly = QtGui.QHBoxLayout()
        self.preview_ly.setObjectName("preview_ly")
        self.horizontalLayout.addLayout(self.preview_ly)
        spacerItem1 = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem1)
        self.verticalLayout_2.addWidget(self.groupBox)
        self.groupBox_3 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_3.setObjectName("groupBox_3")
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.groupBox_3)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.info_lb = QtGui.QLabel(self.groupBox_3)
        self.info_lb.setObjectName("info_lb")
        self.verticalLayout_3.addWidget(self.info_lb)
        self.verticalLayout_2.addWidget(self.groupBox_3)
        self.groupBox_2 = QtGui.QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName("groupBox_2")
        self.verticalLayout = QtGui.QVBoxLayout(self.groupBox_2)
        self.verticalLayout.setObjectName("verticalLayout")
        self.comment_te = QtGui.QTextEdit(self.groupBox_2)
        self.comment_te.setObjectName("comment_te")
        self.verticalLayout.addWidget(self.comment_te)
        self.verticalLayout_2.addWidget(self.groupBox_2)
        self.upload_cbx = QtGui.QCheckBox(self.centralwidget)
        self.upload_cbx.setChecked(True)
        self.upload_cbx.setObjectName("upload_cbx")
        self.verticalLayout_2.addWidget(self.upload_cbx)
        self.publish_btn = QtGui.QPushButton(self.centralwidget)
        self.publish_btn.setObjectName("publish_btn")
        self.verticalLayout_2.addWidget(self.publish_btn)
        publish_dialog.setCentralWidget(self.centralwidget)

        self.retranslateUi(publish_dialog)
        QtCore.QMetaObject.connectSlotsByName(publish_dialog)

    def retranslateUi(self, publish_dialog):
        publish_dialog.setWindowTitle(QtGui.QApplication.translate("publish_dialog", "Publish to Animagrad", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox.setTitle(QtGui.QApplication.translate("publish_dialog", "Preview", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_3.setTitle(QtGui.QApplication.translate("publish_dialog", "Info", None, QtGui.QApplication.UnicodeUTF8))
        self.info_lb.setText(QtGui.QApplication.translate("publish_dialog", "info", None, QtGui.QApplication.UnicodeUTF8))
        self.groupBox_2.setTitle(QtGui.QApplication.translate("publish_dialog", "Comment", None, QtGui.QApplication.UnicodeUTF8))
        self.upload_cbx.setText(QtGui.QApplication.translate("publish_dialog", "Upload Now", None, QtGui.QApplication.UnicodeUTF8))
        self.publish_btn.setText(QtGui.QApplication.translate("publish_dialog", "Publish", None, QtGui.QApplication.UnicodeUTF8))

