from PySide.QtCore import *
from PySide.QtGui import *
import os
import amg_client
import amg_config


class AMGPackerTree(QTreeWidget):
    messageSignal = Signal(str)
    def __init__(self, is_studio):
        super(AMGPackerTree, self).__init__()
        self.is_studio = is_studio
        self.header().hide()

    def update_tree(self, path):
        if not path:
            self.messageSignal.emit('Project path not exists')
            return False
        # root = amg_config.conf['projects_path']
        files = self.collect_studio_files(path)
        self.clear()
        for elem in files:
            elements = elem.in_project_path()
            # print elements
            self.add_element(elem, [elem.project.code]+elements.strip('/').split('/'))
        return True

    def collect_studio_files(self, root):
        client = amg_client.AMGOutsourceClient()
        return client.get_all_assets(root)

    def add_element(self, element, branches, parent=None):
        parent = parent or self.invisibleRootItem()
        b = branches.pop(0)
        for i in range(parent.childCount()):
            ch = parent.child(i)
            if ch.text(0) == b:
                if branches:
                    self.add_element(element, branches, ch)
                    break
        else:
            item = QTreeWidgetItem(parent)
            item.setText(0, b)
            if branches:
                self.add_element(element, branches, item)
            else:
                item.setData(0, 32, element)