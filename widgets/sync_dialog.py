# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from PySide.QtCore import *
from PySide.QtGui import *
import sync_files_dialog_UIs
reload(sync_files_dialog_UIs)
import os


class AMGSyncDialog(QDialog, sync_files_dialog_UIs.Ui_sync_dialog):
    Do_Sync_Signal = Signal(object)
    def __init__(self, dl=None, ul=None, parent=None):
        super(AMGSyncDialog, self).__init__(parent)
        self.setupUi(self)

        self.dl_files = []
        self.ul_files = []

        self.list_download_lw = SyncFileList()
        self.list_download_ly.addWidget(self.list_download_lw)
        self.list_download_lw.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.list_upload_lw = SyncFileList()
        self.list_upload_ly.addWidget(self.list_upload_lw)
        self.list_upload_lw.setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.fill(dl, ul)
        self.sync_btn.clicked.connect(self.accept)

    def fill(self, dl, ul):
        self.list_download_lw.clear()
        for file in dl:
            self.add_item(file, self.list_download_lw)
            self.dl_files.append(file)
        self.list_upload_lw.clear()
        for file in ul:
            self.add_item(file, self.list_upload_lw)
            self.ul_files.append(file)



    def add_item(self, meta, ls):
        it = QListWidgetItem(ls)
        it.setFlags(Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsUserCheckable)
        it.setCheckState(Qt.Checked)
        it.setText(meta.file)
#         tt = '''File: {filename}
# Uploaded: {upl}
# Comment: {comment}'''.format(filename=os.path.basename(meta.path),
#                              upl = meta['uploaded_at'],
#                              comment = meta['comment'])
#         it.setToolTip(tt)
        # it.setData(32, meta)

    def get_files(self):
        dl = [self.list_download_lw.indexFromItem(self.list_download_lw.item(x)).row() for x in range(self.list_download_lw.count()) if self.list_download_lw.item(x).checkState() == Qt.Checked]
        ul = [self.list_upload_lw.indexFromItem(self.list_upload_lw.item(x)).row() for x in range(self.list_upload_lw.count()) if self.list_upload_lw.item(x).checkState() == Qt.Checked]
        # ul = [self.list_upload_lw.item(x).data(32) for x in range(self.list_upload_lw.count()) if self.list_upload_lw.item(x).checkState() == Qt.Checked] or None
        return [self.dl_files[x] for x in dl], [self.ul_files[x] for x in ul]






class SyncFileList(QListWidget):
    def __init__(self):
        super(SyncFileList, self).__init__()

    def keyPressEvent(self, event):
        if event.key() == Qt.Key_Space:
            event.ignore()
            sel = self.selectedItems()
            if sel:
                state = sel[0].checkState()
                if state == Qt.Checked:
                    st = Qt.Unchecked
                else:
                    st = Qt.Checked
                for s in sel:
                    s.setCheckState(st)
            return
        super(SyncFileList, self).keyPressEvent(event)

class ConnectWaitingDialog(QWidget):
    def __init__(self, parent=None):
        super(ConnectWaitingDialog, self).__init__(parent)
        # self.setWindowFlags(Qt.Dialog | Qt.Desktop)
        self.setWindowTitle('Connection')
        lb = QLabel('Connect to server...')
        ly = QVBoxLayout()
        lb.setAlignment(Qt.AlignHCenter | Qt.AlignVCenter)
        self.setLayout(ly)
        ly.addWidget(lb)
        self.setFixedSize(QSize(300,70))
        if parent:
            par_point = parent.geometry().center()
            pt = par_point - QPoint(self.width()/2, self.height()/2)
            self.move(pt)

