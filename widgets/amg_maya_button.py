# -*- coding: utf-8 -*-
from PySide.QtCore import *
from PySide.QtGui import *
import maya.OpenMayaUI as omui
from shiboken import wrapInstance as wrap
qMayaWindow = wrap(long(omui.MQtUtil.mainWindow()), QMainWindow)
from pymel.core import *
from amg import icons as amg_icons
from .. import amg_client
reload(amg_client)
from .. import icons
reload(icons)
from . import open_dialog, sync_dialog, publish_dialog
reload(open_dialog)
reload(sync_dialog)
reload(publish_dialog)
import amg_config
reload(amg_config)


class AMG_ButtonClass(QPushButton):
    objname = 'amg_button'
    def __init__(self):
        super(AMG_ButtonClass, self).__init__(qMayaWindow)
        self.setObjectName(self.objname)
        self.setText('')
        self.setIcon(QIcon(amg_icons.ico['animagrad']))
        self.setFixedSize(QSize(40,40))
        self.setIconSize(QSize(35,35))
        self.clicked.connect(self.click)
        # self.setStyleSheet(style)
        # self.setToolTip(tooltip)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.openMenu)

    @classmethod
    def insert(cls):
        old = qMayaWindow.findChild(QPushButton, cls.objname)
        if old:
            old.setParent(None)
        btn = cls()
        hideButton = qMayaWindow.findChild(QPushButton,'mayaWebButton')
        hideButton.setVisible(0)
        tb = qMayaWindow.findChildren(QToolBar, 'toolBar7')[0]
        tb.addWidget(btn)

    def remove(self):
        hideButton = qMayaWindow.findChild(QPushButton,'mayaWebButton')
        hideButton.setVisible(1)
        self.close()
        self.deleteLater()

    def click(self):
        menu = AMGMenuWidget(self)
        # pos = self.mapToGlobal(self.geometry().bottomLeft())
        pos = QCursor.pos()
        menu.exec_(pos)
        # menu.setStyle(customMenuStyle())
        # menu.setStyleSheet(style)
        # menu.exec_(QCursor.pos())

    def openMenu(self):
        menu = QMenu()
        menu.addAction(QAction('Open Folder', self))#, triggered=self.openFolder))
        menu.addAction(QAction('Go to aimagrag.com', self))#, triggered=self.setProject))
        menu.addAction(QAction('Exit', self, triggered=self.remove))
        # menu.setStyle(customMenuStyle())
        # menu.setStyleSheet(style)
        menu.exec_(QCursor().pos())

class AMGMenuWidget(QFrame):
    def __init__(self, btn):
        super(AMGMenuWidget, self).__init__(qMayaWindow)
        self.btn = btn
        self.setWindowFlags(Qt.FramelessWindowHint | Qt.Window | Qt.ToolTip)
        self.setMinimumWidth(230)
        self.ly = QVBoxLayout()
        self.ly.setContentsMargins(0,0,0,0)
        self.setLayout(self.ly)
        self.setMouseTracking(True)
        self.client = amg_client.AMGOutsourceClient()
        self.build()
        self.setStyleSheet('border-style: outset;border-width: 1px;border-color: black;')

    def build(self):
        self.ly.addWidget( AMGMenuItem('Open Scene', icons.ico['open'], triggered=lambda :(self.open_scene(), self.close()) ))
        self.ly.addWidget( AMGMenuItem('Save +', icons.ico['save'], triggered=lambda :(self.save_plus(), self.close()) ))
        self.ly.addWidget( AMGMenuItem('Publish to AMG', icons.ico['publish'], triggered=lambda :(self.publish_scene(), self.close()) ))
        self.ly.addWidget( AMGMenuItem('Server Sync', icons.ico['sync'], triggered=lambda :(self.sync(), self.close()) ))
        self.ly.addWidget( AMGMenuItem('Help', icons.ico['help'], triggered=lambda :(self.help(), self.close())))

    def exec_(self, pos):
        self.show()
        pos = pos + QPoint(0, -self.height())#+AMGMenuItem.icon_size)
        self.move(pos)
        self.setFocus()
        self.activateWindow()

    def focusOutEvent(self, *args, **kwargs):
        super(AMGMenuWidget, self).focusOutEvent(*args, **kwargs)
        self.close()

    def itemCliched(self, item):
        self.close()

    # actions
    def open_scene(self):
        if cmds.file(q=True, modified=True):
            if not confirmBox('Unsaved changes', 'Scene is modified.\nContinue without saving?'):
                return
        reload(open_dialog)
        self.dial = open_dialog.AMGOpenDialog(qMayaWindow, ['ma', 'mb'])
        if self.dial.exec_():
            d = self.dial.get_data()
            openFile(d.path, f=1)

    def publish_scene(self):
        name = sceneName()
        if not name:
            PopupError('Scene not opened')
            return
        else:
            # create preview
            reload(publish_dialog)
            self.dial = publish_dialog.PublishDialog(name, self.client.publish_file, qMayaWindow)
            def autoupload(val):
                if val:
                    self.sync(True)
            self.dial.closeSignal.connect(autoupload)
            if self.dial.can_publish:
                self.dial.show()

    def save_plus(self):
        name = sceneName()
        if not name:
            PopupError('Scene not saved')
            return
        self.client.save_new_version(name)

    def sync(self, force=False):
        reload(sync_dialog)
        self.hide()
        self.repaint()
        dial = sync_dialog.ConnectWaitingDialog(qMayaWindow)
        dial.show()
        dial.repaint()
        res = False
        # try:
        res = self.client.sync_ftp()
        # except:
        #     pass
        if not res:
            dial.close()
            PopupError('Cant connect to server')
        else:
            dial.close()
            if not any(res):
                informBox('AMG Sync', 'Server is synchronized', 'OK')
                return
            if not force:
                self.sdial = sync_dialog.AMGSyncDialog(res[0], res[1], qMayaWindow)
                if self.sdial.exec_():
                    dl, ul = self.sdial.get_files()
                    self.client.sync_do(dl, ul, res[2])
            else:
                self.client.sync_do(None, res[1], res[2])

    def help(self):
        import webbrowser
        webbrowser.open((amg_config.conf or amg_config.conf)['amg_help_url'])

class AMGMenuItem(QFrame):
    triggered = Signal(object)
    icon_size = 48
    def __init__(self, text, icon, triggered=None):
        super(AMGMenuItem, self).__init__()
        self.action = text
        self.callback = triggered
        self.ly = QHBoxLayout(self)
        self.ly.setSpacing(10)
        self.setMouseTracking(True)
        self.icon = QLabel()
        self.icon.setPixmap(QPixmap(icon).scaled(48,48, Qt.KeepAspectRatio, Qt.SmoothTransformation))
        self.icon.setFixedSize(QSize(self.icon_size, self.icon_size))
        self.text = QLabel(text)
        font = QFont()
        font.setPointSize(14)
        font.setBold(True)
        self.text.setFont(font)
        self.text.setAutoFillBackground(True)
        self.style_on  = 'border-width: 0px;background: #595959;'
        self.style_off = 'border-width: 0px;'
        self.setStyleSheet(self.style_off)
        self.ly.addWidget(self.icon)
        self.ly.addWidget(self.text)

    def enterEvent(self, event):
        self.setStyleSheet(self.style_on)
        super(AMGMenuItem, self).enterEvent(event)

    def leaveEvent(self, event):
        self.setStyleSheet(self.style_off)
        super(AMGMenuItem, self).leaveEvent(event)

    def mouseReleaseEvent(self, *args, **kwargs):
        if self.callback:
            self.callback()
        super(AMGMenuItem, self).mouseReleaseEvent(*args, **kwargs)
