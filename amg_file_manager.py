# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os, sys, re, shutil, json, datetime, time, ftplib, io
from StringIO import StringIO
from amg import api
from amg.api.amg_path import join
from amg import icons
from amg.shotgun import amg_shoutgun as ams
import amg_config


class Meta(object):
    suffix = '.meta'
    preview_suffix = '.preview.jpg'
    # owner
    STUDIO = 'studio'
    USER = 'user'
    # actions
    DELETE = 'delete'
    DOWNLOAD = 'download'
    UPLOAD = 'upload'
    meta_data = dict(
        owner=None,         # owner of file, studio or user
        artist=None,        # hwo is work with scene
        action=None,        # action for user side (delete,...)
        modify_at=None,     # time stamp of last changes
        uploaded_at=None,   # upload time stamp
        opened=False,       # file is new or already opened
        uploaded=False,     # file is uploaded to ftp (user side)
        task_start=None,    # start time
        task_end=None,      # deadline of task
        is_task_file=False, # is file from task or depended file
        comment=''          # comment
        )
    def __init__(self, path, ftp=None):
        super(Meta, self).__init__()
        if not path.endswith(self.suffix):
            raise Exception('Wrong meta file name %s' % path)
        self.path = path
        self.ftp = ftp
        self.is_ftp = bool(ftp)
        self.__data = {}
        self.file = self.path[:-len(self.suffix)].replace('\\','/')
        if self.is_ftp:
            if not ftp:
                raise Exception('Need to set FTP handler')
            self.data = self.__read_from_ftp(ftp)
            try:
                self.ftp.size(self.file)
            except:
                self.file = None
        else:
            self.data = self.__read_from_local()
            if not os.path.exists(self.file):
                print 'file not exists'
                self.file = None
        self.preview = self.path[:-len(self.suffix)].replace('\\','/') + self.preview_suffix
        if not os.path.exists(self.preview):
            self.preview = None

    def __setitem__(self, key, value):
        self.__data[key] = value

    def __getitem__(self, item):
        return self.__data.get(item)

    def __repr__(self):
        text = '\n%s\n' % (self.file or self.path)
        text += '\n'.join(['\t%s : %s' % (k, v) for k,v in self.__data.items()])+'\n'
        return text

    @classmethod
    def create_for_local_file(cls, path, ftp=None):
        metafile = path+cls.suffix
        c = cls(metafile, ftp)
        return c

    @property
    def data(self):
        return self.__data
    @data.setter
    def data(self, value):
        self.__data.update(value)

    def copy(self, other_meta):
        self.__data = other_meta.data

    def __read_from_ftp(self, ftp):
        try:
            r = StringIO()
            ftp.retrbinary('RETR %s' % self.path, r.write)
            r.seek(0)
            data = json.loads(r.getvalue())
        except:
            data = self.meta_data.copy()
        return data

    def __read_from_local(self):
        if os.path.exists(self.path):
            meta = json.load(open(self.path))
        else:
            meta = self.meta_data.copy()
        if self.file:
            meta['modify_at'] = int(os.stat(self.file).st_mtime)
        # preview
        # todo: download preview?
        return meta

    def __save_to_ftp(self, ftp_path=None):
        if not self.ftp:
            raise Exception('FTP not set')
        self.ftp.cwd('/')
        dir = os.path.dirname(ftp_path or self.path)
        self.ftp.cwd(dir)
        bio = io.BytesIO(json.dumps(self.data, indent=2))
        self.ftp.storbinary('STOR %s' % (ftp_path or os.path.basename(self.path)), bio)
        # preview
        if not self.preview or not os.path.exists(self.preview):
            return
        ftp_path = ftp_path[:-len(self.suffix)].replace('\\','/') + self.preview_suffix
        self.ftp.storbinary('STOR %s' % (ftp_path or os.path.basename(self.preview)), open(self.preview, 'rb'))

    def __save_to_local(self):
        # self.data['opened'] = True
        json.dump(self.data, open(self.path, 'w'), indent=2)
        if self.preview:
            local_path = self.path[:-len(self.suffix)].replace('\\','/') + self.preview_suffix
            if not local_path == self.preview:
                # shutil.copyfile(self.preview, local_path)
                open(local_path, 'wb').write(open(self.preview, 'rb').read())

    def save(self):
        if self.is_ftp:
            self.__save_to_ftp()
        else:
            self.__save_to_local()

    def copy_local_file(self, root):
        if self.is_ftp:
            json.dump(self.data, open(root + self.path, 'w'), indent=2)

    def copy_to_ftp(self, ftp_dir, ftp):
        if not self.is_ftp:
            self.ftp = ftp
            full_name = '/'.join([ftp_dir, os.path.basename(self.path)])
            self.__save_to_ftp(full_name)

    def add_preview(self, file_or_pixmap):
        if isinstance(file_or_pixmap, (str, unicode)):
            if os.path.exists(file_or_pixmap):
                self.preview = file_or_pixmap
            else:
                print 'File not found'
        else:
            if self.file:
                preview_path = self.file + self.preview_suffix
                res = file_or_pixmap.save(preview_path, 'JPG', 90)
                if res:
                    self.preview = preview_path
                else:
                    print 'Error save image'
            else:
                print 'original file not exists'

    def move_to_file(self, dest, options=None):
        new_path = dest + self.suffix
        self.path = new_path
        self.file = dest
        if options:
            self.data.update(options)
        if self.preview:
            new_preview = dest + self.preview_suffix
            open(new_preview, 'wb').write(open(self.preview, 'rb').read())
            self.preview = new_preview
        self.save()


class FM(object):
    ext = None
    def __init__(self, file):
        super(FM, self).__init__()
        self.path = file.replace('\\','/')
        self.project = ams.SG_Project.from_path(file)
        self.meta = Meta.create_for_local_file(self.path)
        # stuff
        self.stuff = Stuff(self.path, self)
        #icon
        self.icon = icons.extico.get(os.path.splitext(file)[-1].strip('.'), icons.extico['default'])

    def __new__(cls, file):
        if not os.path.exists(file):
            print 'File not exists: %s' % file
            return None
        project = ams.SG_Project.from_path(file)
        if not project:
            print 'Project not found'
            return
        return super(FM, cls).__new__(cls)

    def __repr__(self):
        return '<FM> ' + self.path

    @staticmethod
    def create(file):
        if isinstance(file, Meta):
            file = file.file
        ext = os.path.splitext(file)[-1].strip('.')
        if False:
            return FM(file)
        for name, cls in globals().items():
            if name.startswith('FM_') and cls.ext == ext:
                result = cls(file)
                break
        else:
            result = FM(file)
        return result

    def extract_dependencies(self):
        return []

    def repath_to_user(self, user, comment=''):
        if self.is_studio_side():
            dest = self.get_ftp_path(user)
            # repath and copy file
            self.copy_file(self.path, dest, user)
            # crate studio meta
            local_meta = Meta.create_for_local_file(self.path)
            local_meta['owner'] = Meta.STUDIO
            local_meta['artist'] = user.username
            local_meta['comment'] = comment
            local_meta['uploaded_at'] = timestamp()
            local_meta.save()
            # create ftp meta
            ftp_meta = Meta.create_for_local_file(dest)
            ftp_meta.copy(local_meta)
            ftp_meta.save()
        else:
            print 'File on user side'

    def repath_to_studio(self, user):
        if self.is_user_side():
            studio_path = join(self.project.path(), self.in_project_path())
            if os.path.exists(studio_path):
                self.backup_file(studio_path)
            self.copy_file(self.path, studio_path, user)
            self.meta['opened'] = True
            self.meta.save()
            self.create_from_user_to_studio_meta(studio_path)
        else:
            print 'File on studio side'

    def create_from_user_to_studio_meta(self, studio_path):
        # studio_path = join(self.project.path(), self.in_project_path())
        # self.meta['owner'] = self.user.username
        # self.meta['opened'] = False
        self.meta.path = studio_path + Meta.suffix
        self.meta.save()

    def copy_file(self, src, trg, user):
        if not os.path.exists(src):
            print 'Error file %s' % src
            return
        d = os.path.dirname(trg)
        if not os.path.exists(d):
            os.makedirs(d)
        shutil.copy2(src, trg)
        shutil.copystat(src, trg)

    def extract(self):
        if not self.path:
            return []
        return self.extract_dependencies()

    def is_studio_side(self):
        if self.path.startswith(self.project.path()):
            return True
        return False

    def is_user_side(self):
        if not self.user:
            return False
        if self.path.startswith(self.user.studio_path):
            return True
        return False

    def remove_from_user(self):
        path = self.get_ftp_path()
        if os.path.exists(path):
            try:
                os.remove(path)
            except:
                print 'Error: Cant remove path: %s' % path

    ### PATH
    def get_user_path(self, user):
        return '/'.join([user.local_path, self.in_project_path()])

    def get_ftp_path(self, user):
        project_folder = os.path.basename(self.project.path())
        user_ftp_path = join(user.studio_path, project_folder, self.in_project_path())
        return user_ftp_path

    def get_studio_path(self):
        return '/'.join([self.project.path(), self.in_project_path()])

    def in_project_path(self):
        return self.path.rsplit(self.project.code, 1)[-1].strip('/')

    ### META

    def _create_meta_file(self, data=None):
        self.meta = Meta.create_for_local_file(self.get_ftp_path())
        if data:
            self.meta.data.update(data)
        else:
            self.meta['owner'] = Meta.STUDIO
        self.meta['modify_at'] = int(os.stat(self.path).st_mtime)
        self.meta['uploaded_at'] = timestamp()
        # self.meta['action'] = 'download'
        self.meta['opened'] = None
        self.meta['uploaded'] = None
        # self.meta.save()
        return self.meta

    def add_comment_to_meta(self, comment):
        meta = self.get_meta()
        meta['comment'] = comment
        meta.save()

    def remove(self):
        path = self.get_ftp_path()
        if os.path.exists(path):
            try:
                os.remove(path)
            except:
                print 'Error: Cant remove path: %s' % path
        #remove empty folders
        meta = self.get_meta()
        if meta['owner'] == Meta.STUDIO:
            os.remove(meta.path)
        else:
            meta['action'] = 'delete'
            meta.save()

    @staticmethod
    def backup_file(path):
        backup_folder = join(os.path.dirname(path), 'backup')
        if not os.path.exists(backup_folder):
            os.makedirs(backup_folder)
        new_path = join(os.path.dirname(path), 'backup', os.path.basename(path)+'.backup')
        def inc_name(name):
            digits = re.findall(r"\d+$", name)
            if not digits:
                return name+'1'
            else:
                num = int(digits[0])
                name = name[:-len(digits[0])] + str(num+1)
                return name
        while os.path.exists(new_path):
            incName = inc_name(os.path.basename(new_path))
            new_path = join(os.path.dirname(path), 'backup', incName)
        shutil.move(path, new_path)

    def get_meta(self):
        return self.meta or Meta.create_for_local_file(self.get_ftp_path())

    def set_meta(self, data):
        path = self.path + '.meta'
        if os.path.exists(path):
            _data = json.load(open(path))
        else:
            _data = {}
        _data.update(data)
        json.dump(_data, open(path, 'w'), indent=2)

    # STUFF
    def elements(self):
        # prj = self.project.data['tank_name'] or 'NoProject'
        prj = self.project.name or 'No Project'
        return [prj]+self.stuff.context['pipeline_path'].split('|')+[os.path.basename(self.path)]


class FM_ma(FM):
    ext = 'ma'
    def __init__(self, file):
        super(FM_ma, self).__init__(file)

    def extract_dependencies(self):
        src = open(self.path).read()
        studio_local = api.amg_path.studio_projects_path()
        included = re.findall(r'"(%s.*?)"' % studio_local, src)
        included = [x.replace('\\','/').strip().rstrip('/') for x in included]
        return included

    def copy_file(self, src_file, trg_file, user):
        src_path = os.path.dirname(self.project.path()).replace('\\','/')
        trg_path = user.local_path.replace('\\','/')
        self.repath_ma(src_file, trg_file, src_path, trg_path)

    def repath_to_studio(self, user):
        src_file = self.path
        trg_file = join(self.project.path(), self.in_project_path())
        trg_path = amg_config.conf.get('remap_projects_path') or os.path.dirname(self.project.path()).replace('\\','/')
        src_path= user.local_path.replace('\\','/')
        self.repath_ma(src_file, trg_file, src_path, trg_path)
        self.meta['opened'] = True
        self.meta.save()
        self.meta.move_to_file(trg_file, {'opened':False})

        # self.meta.path = self.path + Meta.suffix
        # studio_meta = Meta.create_for_local_file(trg_file)
        # studio_meta.copy(self.meta)
        # studio_meta['opened'] = False
        # studio_meta.save()


    def repath_ma(self, src_file, trg_file, src_path, trg_path):
        dir = os.path.dirname(trg_file)
        if not os.path.exists(dir):
            os.makedirs(dir)
        print 'SOURCE FILE', src_file
        print 'SAVE OT:', trg_file
        print 'FINE PATH', src_path
        print 'REPLACE TO', trg_path
        with open(trg_file, 'w') as tf:
            with open(src_file) as sf:
                for line in sf:
                    try:
                        line = line.replace(src_path, trg_path)
                    except:
                        pass
                    tf.write(line)
        try:
            shutil.copystat(src_file, trg_file)
        except:
            print 'Cant copy stat for: %s' % trg_file


class FM_mb(FM_ma):
    ext = 'mb'
    def __init__(self, file):
        super(FM_ma, self).__init__(file)
        # raise Exception('not ready')

    def extract_dependencies(self):
        return []
        # todo: get files with command line application
        text = open(self.path, 'rb').read()
        s = re.findall(r'(((//[\w.]+)|\w:)/[\w/]+\.\w+)', text)
        if s:
            out = [x[0] for x in s]
            return out
        return []


class FM_hip(FM):
    ext = 'hip'
    def __init__(self, file):
        super(FM_hip, self).__init__(file)
        raise Exception('not ready')

    def extract_dependencies(self):
        return []
        # todo: get files with command line application
        text = open(self.path, 'rb').read()
        # variable
        variables = dict(re.findall(r"set\s+-g\s+(\w+)\s=\s'(.+)'", text))
        # filepath
        match = [x.strip() for x in re.findall(r"\w+\s+\[.*?\]\s+\((.*?)\)",text) if x.strip()]
        paths_from_file = []
        for f in match:
            vars = sorted(re.findall(r"\$([A-Z0-9_]+)", f), reverse=1, key=lambda x:len(x))
            if vars:
                for v in vars:
                    if v in variables:
                        f = f.replace('$'+v, variables[v])
            paths_from_file.append(f)
        # assets
        otls = [x[0] for x in re.findall(r"([\w/:.]+\.(hda|otl))", text)]
        otls = [x for x in otls if os.path.exists(x)]
        path_list = [x for x in paths_from_file if re.match(r'(((//[\w.]+)|\w:)/[\w/<>$]+\.\w+)', x)]
        result = otls + path_list
        return result


class FM_nk(FM):
    ext = 'nk'
    def __init__(self, file):
        super(FM_nk, self).__init__(file)
        raise Exception('not ready')

    def extract_dependencies(self):
        result = []
        lines = open(self.path).readlines()
        for l in lines:
            res = re.findall(r"file\s+(((//[\w.]+)|\w:)/[\w/%#]+\.\w+)", l)
            if res:
                for f in res:
                    result.append(f[0])
        return result

    def repath(self, user):
        FM_ma.repath(self, user)


class Stuff(object):
    # types
    MAYA_SCENE = 1
    NUKE_SCRIPT = 2
    HOUDINI_SCENE = 3
    MAX_SCENE = 4
    IMAGE = 5
    TEXTURE = 6
    TEXT = 7
    OTHER = 99
    # stuff
    IS_ASSET = 'assets'
    IS_SEQUENCE = 'sequences'
    IS_OTHER = 'other'

    def __init__(self, path, parent):
        self.context = dict(
            path=path,
            entity=self.IS_OTHER,
            pipeline_path = 'other'
        )
        self.parent = parent
        self.type = None
        self.__define(path)

    def __define(self, path):
        # type
        name, ext = os.path.splitext(os.path.basename(path))
        if ext in ['.mb', '.ma']:
            self.type = self.MAYA_SCENE
        elif ext == '.hip':
            self.type = self.HOUDINI_SCENE
        elif ext == '.nk':
            self.type = self.NUKE_SCRIPT
        elif ext == '.max':
            self.type = self.MAX_SCENE
        elif ext in ['.txt', '.doc', '.docx']:
            self.type = self.TEXT
        elif Texture.is_texture(path):
            self.type = self.TEXTURE
        else:
            self.type = self.OTHER

        # stuff
        s = re.match(r".*?/sequences/(\w+)/([\w-]+)/(\w+)/(\w+)/(\w+)/([a-zA-Z0-9-_.]+?)(\.\w+)$" , path)
        if s:
            self.context['episode'], self.context['shot'], self.context['step'] =  s.groups()[:3]
            self.context['entity'] = self.IS_SEQUENCE
            self.context['pipeline_path'] = '|'.join(['sequences', self.context['episode'], self.context['shot'], self.context['step']])
            return
        a = re.match(r".*?/assets/(\w+)/([\w-]+)/(\w+)/(\w+)/(\w+)/([a-zA-Z0-9-_.]+?)(\.\w+)$", path)
        if a:
            self.context['type'], self.context['name'], self.context['step'] =  a.groups()[:3]
            self.context['entity'] = self.IS_ASSET
            self.context['pipeline_path'] = '|'.join(['assets', self.context['type'], self.context['name'], self.context['step']])
            return
        self.context['pipeline_path'] = '|'.join(self.context['path'].split(self.parent.project.code)[-1].strip('/').split('/')[:-1])


    def __repr__(self):
        return json.dumps(self.context)




class Texture(object):
    @classmethod
    def is_texture(cls, path):
        return bool(re.match(r".*?(\d{4}).(tif|tx)", os.path.basename(path)))

    @classmethod
    def correct_name(cls, path):
        return True


def timestamp():
    return  int(time.mktime(datetime.datetime.now().timetuple()))

def stamp_to_str(stamp):
    return datetime.datetime.fromtimestamp(stamp).strftime('%Y.%m.%d %H:%M:%S')


types = dict(
    other='Other',
    ma='Maya ASCII',
    mb='Maya Binary',
    nk='Nuke',
    hip='Houdini',
    exr='OpenEXR',
    tif='TIFF',
    tiff='TIFF',
    jpg='JPEG Image',
    jpeg='JPEG Image',
    png='PNG Image',
    ass='Arnold Scene',
    abc='Alembic'
)