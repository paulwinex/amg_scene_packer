'''menuData
act={name:Animagrad UI,action:show()}
'''

def show_client():
    ctx = None
    try:
        import pymel
        ctx = 'maya'
    except:
        pass
    try:
        import hou
        ctx = 'hou'
    except:
        pass
    try:
        import nuke
        ctx = 'nuke'
    except:
        pass
    if ctx:
        if ctx == 'maya':
            show_maya()
        elif ctx == 'hou':
            show_hou()
        elif ctx == 'nuke':
            show_nuke()
    else:
        print 'Context not found'


def show_maya():
    from widgets import amg_maya_button
    reload(amg_maya_button)
    amg_maya_button.AMG_ButtonClass.insert()

def show_hou():
    pass

def show_nuke():
    pass