# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import ftpsync
import amg_file_manager
reload(amg_file_manager)
import amg_config
from amg.shotgun import amg_shoutgun as ams
import os

ctx = None
try:
    from pymel.core import *
    ctx = 'maya'
except:
    pass
try:
    import hou
    ctx = 'hou'
except:
    pass
try:
    import nuke
    ctx = 'nuke'
except:
    pass


class AMGOutsourceClient(object):
    def __init__(self):
        super(AMGOutsourceClient, self).__init__()

    def save_new_version(self, filepath):
        base = os.path.basename(filepath)
        dir = os.path.dirname(filepath)
        v = re.match(r"(.*?v)(\d+)(\.\w+)", base)
        if v:
            nm, ver, ext = v.groups()
            new = nm + str(int(ver)+1).zfill(len(ver)) + ext
            if ctx == 'maya':
                if confirmBox('Save new version', 'Save scene to new version?\n%s' % new):
                    saveAs(os.path.join(dir, new))
            elif ctx == 'nuke':
                nuke.scriptSaveAs(new, True)
            elif ctx == 'hou':
                if os.path.exists(new):
                    os.remove(new)
                hou.hipFile.save(new, True)
            print 'Saved', os.path.join(dir, new).replace('\\','/')
        else:
            if ctx == 'maya':
                PopupError('Version not found\n%s' % base)


    def publish_file(self, meta, comment, preview):
        if not isinstance(meta, amg_file_manager.Meta):
            if not os.path.exists(meta):
                raise Exception('Wrong file path')
            meta = amg_file_manager.Meta.create_for_local_file(meta)

        meta['comment'] = comment
        meta['owner'] = amg_config.conf['username']
        meta['uploaded'] = False
        meta.save()

        if preview: # QPixmap
            meta.add_preview(preview)
        return True

    def get_files(self, projects_root, file_filter=None):
        skip_always = ['mel', 'tmp', 'backup', 'autosave', 'meta', 'nk~']
        sg_projects = ams.SG.get_all_projects()
        elements = []
        if not os.path.exists(projects_root) or not os.path.isdir(projects_root):
            print 'Wrong projects path: %s\nEdit user config and try again.' % projects_root
            return
        for project in os.listdir(projects_root):
            prjpath = os.path.join(projects_root, project)
            if not os.path.isdir(prjpath):
                continue
            # prj = ([x for x in sg_projects if x.data['tank_name'] == project] or [None])[0]
            # if not prj:
            #     continue
            for path, dirs, files in os.walk(prjpath):
                for f in files:
                    ext = os.path.splitext(f)[-1].strip('.')
                    if file_filter:
                        if not ext in file_filter:
                            continue
                    if f.endswith(amg_file_manager.Meta.preview_suffix): # skip_preview
                        continue
                    if ext in skip_always:
                        continue
                    p = os.path.join(path, f).replace('\\','/')
                    fm = amg_file_manager.FM(p)
                    if fm:
                       elements.append(fm)
        return elements


    def sync_ftp(self, force=False):
        amg_config.conf = amg_config.get()
        if not amg_config.conf:
            print 'User config not found'
            return False
        url = amg_config.conf.get('amg_ftp_url')
        usr = amg_config.conf.get('username')
        pwd = amg_config.conf.get('password')
        if not all([url,usr,pwd]):
            print 'Wrong user config. Set amg_ftp_path, username and password'
            return False

        self.ftp = ftpsync.FTP_Clone(url,usr,pwd)
        err = self.ftp.login()
        if not err:
            print 'Wrong authentication data'
            return False
        to_dl, prj_meta = self.ftp.check_to_download()
        to_ul = self.ftp.check_to_upload()
        if force:
            # self.ftp.sync_files(download_files=to_dl, upload_files=to_ul)
            # self.ftp.logout()
            self.sync_do(to_dl, to_ul, prj_meta)
            self.ftp.logout()
            return True, True
        else:
            self.ftp.logout()
            return to_dl, to_ul, prj_meta

    def sync_do(self, to_dl, to_ul, prj_meta):
        url = amg_config.conf.get('amg_ftp_url')
        usr = amg_config.conf.get('username')
        pwd = amg_config.conf.get('password')
        self.ftp = ftpsync.FTP_Clone(url,usr,pwd)
        if not self.ftp.login():
            print 'Error login', url,usr,'*'*len(pwd)
            return
        if to_dl:
            for d in to_dl:
                if d.is_ftp:
                    d.ftp = self.ftp.ftp
            # for d in to_ul:
            #     if d.is_ftp:
            #         d.ftp = self.ftp.ftp

        self.ftp.sync_files(download_files=to_dl, upload_files=to_ul, project_meta=prj_meta)
        self.ftp.logout()


#
# class AssetScene(object):
#     ASSET = 'Assets'
#     SCENE = 'Sequences'
#
#     def __init__(self, path, type, data):
#         super(AssetScene, self).__init__()
#         self.path = path
#         self.type = type
#         if type == self.ASSET:
#             self.category = data[0]
#             self.name = data[1]
#             self.step = data[2]
#         elif type == self.SCENE:
#             self.episode = data[0]
#             self.shot = data[1]
#             self.step = data[2]
#         else:
#             raise Exception('Unknown type')
#         self.meta = self.path + file_manager.Meta.suffix
#         if not os.path.exists(self.meta):
#             self.meta = None
#         else:
#             self.meta = file_manager.Meta(self.meta)
#
#
#     @classmethod
#     def from_path(cls, path):
#         s = re.match(r".*?/sequences/(\w+)/([\w-]+)/(\w+)/(\w+)/(\w+)/([a-zA-Z0-9-_.]+?)(\.\w+)$" , path)
#         if s:
#             group = s.groups()
#             if group[-1] == '.meta':
#                 return
#             episode, shot, step =  s.groups()[:3]
#             return cls(path, AssetScene.SCENE, [episode, shot, step])
#         a = re.match(r".*?/assets/(\w+)/([\w-]+)/(\w+)/(\w+)/(\w+)/([a-zA-Z0-9-_.]+?)(\.\w+)$", path)
#         if a:
#             group = a.groups()
#             if group[-1] == '.meta':
#                 return
#             type, name, step =  a.groups()[:3]
#             return cls(path, AssetScene.ASSET, [type, name, step])
#
#     def elements(self):
#         if self.type == self.SCENE:
#             return [self.episode, self.shot, self.step, os.path.basename(self.path)]
#         elif self.type == self.ASSET:
#             return [self.category, self.name, self.step, os.path.basename(self.path)]
#
#     def __repr__(self):
#         if self.type == self.SCENE:
#             return '%s/%s/%s/%s' % (self.episode, self.shot, self.step, os.path.basename(self.path))
#         elif self.type == self.ASSET:
#             return '%s/%s/%s/%s' % (self.category, self.name, self.step, os.path.basename(self.path))